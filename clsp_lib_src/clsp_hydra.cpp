#include "clsp_hydra.h"
#include "clsp_random.h"
#include "ConsoleColor.h"
#include "../../HydraAPI/hydra_api/HydraObjectManager.h"
#include "../../HydraAPI/hydra_api/HydraAPI.h"

#include <iostream>
#include <sstream>
#include <cassert>
#include <vector>
#include <map>
#include <experimental/filesystem>


namespace fs = std::experimental::filesystem;

std::vector<CSLP_Material> clspObtainMaterialsFromHydraSceneLibrary(const char* a_path)
{
  hrErrorCallerPlace(L"clspObtainMaterialsFromHydraSceneLibrary");

  std::wstring sceneLibpath = clsp_s2ws(a_path);

  hrSceneLibraryOpen(sceneLibpath.c_str(), HR_OPEN_READ_ONLY);

  auto sceneInfo = hrSceneLibraryInfo();

 // std::cout << "sceneInfo.materialsNum = " << sceneInfo.materialsNum << std::endl;

  std::vector<CSLP_Material> materials;
  materials.reserve(sceneInfo.materialsNum);

  for(int32_t i=0;i<sceneInfo.materialsNum;i++)
  {
    HRMaterialRef mat;
    mat.id = i;

    hrMaterialOpen(mat, HR_OPEN_READ_ONLY);
    {
      auto matNode = hrMaterialParamNode(mat);
      auto meta    = matNode.child(L"metadata");

      if(meta != nullptr)
      {
        CSLP_Material currmat;
        currmat.dbId   = -1; // we can not set this, id ill change when insert it to database
        currmat.name   = clsp_ws2s(matNode.attribute(L"name").as_string());
        currmat.type   = clsp_ws2s(meta.attribute(L"category").as_string());
        currmat.target = clsp_ws2s(meta.attribute(L"target").as_string());
        
        materials.push_back(currmat);
      }

    }
    hrMaterialClose(mat);
  }

  return materials;
}



std::vector<CSLP_3DModel> clspObtainModelsFromFolder(const char* a_path)
{
  hrErrorCallerPlace(L"clspObtainModelsFromFolder");
#ifdef WIN32
  hr_mkdir(L"tempmodels");
#else
  hr_mkdir("tempmodels");
#endif
  hrSceneLibraryOpen(L"tempmodels", HR_WRITE_DISCARD);

  std::vector<CSLP_3DModel> res;
  res.reserve(1000);

  std::string path = a_path;
  for (const auto& entry : fs::recursive_directory_iterator(path))
  {
  #ifdef WIN32
    const std::string& path = entry.path().string();
  #else
    const std::string& path = entry.path();
  #endif

    if(path.find(".vsgf")  != std::string::npos ||
       path.find(".vsgfc") != std::string::npos)
    {
      const std::wstring path2 = clsp_s2ws(path);

      HRMeshRef  mesh = hrMeshCreateFromFileDL(path2.c_str());
      HRMeshInfo info = hrMeshGetInfo(mesh);

      CSLP_3DModel model;

      model.dbId = -1; // can't set this
      for(int i=0;i<3;i++)
      {
        model.boxmin[i] = info.boxMin[i];
        model.boxmax[i] = info.boxMax[i];
      }

      std::vector<std::string> folders;
      {
        std::stringstream test(path);
        std::string segment;
        #ifdef WIN32
        while (std::getline(test, segment, '\\'))
        #else
        while (std::getline(test, segment, '/')) // split single file path to folders
        #endif
        {
          if(segment.find(".vsgf") == std::string::npos && segment.find(".vsgfc") == std::string::npos)
            folders.push_back(segment);
        }
      }
      
      model.filepath    = path;
      model.category    = (folders.size() > 1) ? folders[1] : "undefined"; // get this from file path
      model.subcategory = (folders.size() > 2) ? folders[2] : "undefined"; // get this from file path
      model.style       = "any";                                           // get this from file name
  
      auto styleDelimiter = path.find_last_of("_");
      if(styleDelimiter != std::string::npos)
      {
        auto endPos = path.find_last_of(".vsgf");
        model.style = path.substr(styleDelimiter + 1, endPos - styleDelimiter - 5);
      }
      
      model.matnames.resize(0);
      std::stringstream strOut;
      for (int i = 0; i < info.matNamesListSize; i++)
        model.matnames.push_back(clsp_ws2s(info.matNamesList[i]));
      
      res.push_back(model);
    }
  }


  return res;
}


HRMaterialRef CreateDiffuseMaterialWithTexture(const std::wstring& texpathW)
{
  HRTextureNodeRef texRef = hrTexture2DCreateFromFileDL(texpathW.c_str());
  
  HRMaterialRef matRef = hrMaterialCreate(texpathW.c_str());
  
  hrMaterialOpen(matRef, HR_WRITE_DISCARD);
  {
    auto node = hrMaterialParamNode(matRef);
    
    auto diff = node.append_child(L"diffuse");
    diff.append_attribute(L"brdf_type").set_value(L"lambert");
    
    auto color = diff.append_child(L"color");
    color.append_attribute(L"val").set_value(L"0.85 0.85 0.85");
    color.append_attribute(L"tex_apply_mode").set_value(L"multiply");
    
    hrTextureBind(texRef, color);
  }
  hrMaterialClose(matRef);
  
  return matRef;
}

std::vector<CSLP_Envir> clspListEnvirs(const char* a_path)
{
  auto folders = hr_listfiles(a_path, false);
  std::cout << "clspListEnvirs, folders.size() = " << folders.size() << std::endl;
  
  std::vector<CSLP_Envir> envirs;
  envirs.reserve(1000);
  
  for(const auto& folder : folders)
  {
    const auto posOfLastSlash = folder.find_last_of("/");
    const auto fileSource     = folder.substr(posOfLastSlash+1, folder.size());
    //std::cout << "fileSource = " << fileSource.c_str() << std::endl;
    
    auto files = hr_listfiles(folder.c_str(), false);
    
    for(const auto& file : files)
    {
      const auto posOfLastSlash2 = file.find_last_of("/");
      const auto fileName        = file.substr(posOfLastSlash2+1, file.size());
      
      CSLP_Envir env;
      env.dbId       = -1;
      env.category   = "unknown";
      env.daytime    = "unknown";
      env.fileSource = fileSource.c_str();
      env.path       = std::string("envlib/") + fileSource + "/" + fileName;
      envirs.push_back(env);
    }
  }
  
  return envirs;
}



//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

//std::vector<std::string> hr_listfiles(const std::string &a_folder);

struct CLSP_Randomizer : public ICLSP_Randomizer
{
  CLSP_Randomizer() : m_db(nullptr) {}
  CLSP_Randomizer(const char* a_path) { init(a_path);}

  std::vector<std::string>   ListModelCategories();
  std::vector<CSLP_3DModel>  SelectModels_WhereFitIn(const char* condition, float a_boxSize[3]);
  std::vector<int32_t>       SampleRemapList(const CSLP_3DModel& a_model, HRMeshRef a_mesh, const std::vector<float> rndArray);

protected:

  void init(const char* a_path);

  CSLP_Context*      m_db;
  HRUtils::MergeInfo m_mergeInfo;

  // custom data
  //
  std::vector<int32_t> m_materialsWithoutTextures;
  std::vector<int32_t> m_materialsAll;
  std::vector<int32_t> m_materialsHumanMale;
  std::vector<int32_t> m_materialsHumanFemale;


  const std::vector<std::wstring> targetList = { L"Undefined", L"PCscreen", L"PCkeyboard", L"MacKeyboard", L"SmartphoneScreen",
    L"TabletScreen", L"RulerStraight", L"RulerAngular", L"RulerProtractor", L"Notepad", L"BoxPaperclips", L"BoxStaples", 
    L"PictureHorizontal", L"PictureVertical", L"ClockCircle", L"ClockSquare", L"ClockRecrangular", L"GlossyMetal", L"GlossyMetalHole",
    L"GlossyPlasticDark", L"GlossyPlasticHole", L"Diffuse", L"GlassClear", L"Chrome", L"CalendarOrPosterHoriz", L"Book", L"Whiteboard", 
    L"StorageBox", L"CalendarOrPosterVert", L"ChairLeg", L"CasePC", L"BlackPlastic", L"ChairFabric", L"GlossyPlasticLight", L"Lampshade" };

  std::map<std::wstring, std::vector<int32_t>*> mapTagsTargets;
  std::vector<int32_t> m_targPCscreen;
  std::vector<int32_t> m_targPCkeyboard;
  std::vector<int32_t> m_targMacKeyboard;
  std::vector<int32_t> m_targSmartphoneScreen;
  std::vector<int32_t> m_targTabletScreen;
  std::vector<int32_t> m_targRulerStraight;
  std::vector<int32_t> m_targRulerAngular;
  std::vector<int32_t> m_targRulerProtractor;
  std::vector<int32_t> m_targNotepad;
  std::vector<int32_t> m_targBoxPaperclips;
  std::vector<int32_t> m_targBoxStaples;
  std::vector<int32_t> m_targPictureHorizontal;
  std::vector<int32_t> m_targPictureVertical;
  std::vector<int32_t> m_targClockCircle;
  std::vector<int32_t> m_targClockSquare;
  std::vector<int32_t> m_targClockRecrangular;
  std::vector<int32_t> m_targGlossyMetal;
  std::vector<int32_t> m_targGlossyMetalHole;
  std::vector<int32_t> m_targGlossyPlasticDark;
  std::vector<int32_t> m_targGlossyPlasticHole;
  std::vector<int32_t> m_targDiffuse;
  std::vector<int32_t> m_targGlassClear;
  std::vector<int32_t> m_targChrome;
  std::vector<int32_t> m_targCalendarOrPosterHoriz;
  std::vector<int32_t> m_targBook;
  std::vector<int32_t> m_targWhiteboard;
  std::vector<int32_t> m_targStorageBox;
  std::vector<int32_t> m_targCalendarOrPosterVert;
  std::vector<int32_t> m_targChairLeg;
  std::vector<int32_t> m_targCasePC;
  std::vector<int32_t> m_targBlackPlastic;
  std::vector<int32_t> m_targChairFabric;
  std::vector<int32_t> m_targGlossyPlasticLight;
  std::vector<int32_t> m_targLampshade;

  const std::vector<std::wstring> categoryList = { L"Ceramic", L"Concrete", L"Emission", L"Fabric", L"Glass", L"Leather", L"Liquid", 
    L"Marble", L"Metal", L"Organic", L"Paint", L"Paper", L"Plastic", L"Rubber", L"Wood" };

  std::map<std::wstring, std::vector<int32_t>*> mapTagsMaterials; 
  std::vector<int32_t> m_matCeramic;
  std::vector<int32_t> m_matConcrete;
  std::vector<int32_t> m_matEmission;
  std::vector<int32_t> m_matFabric;
  std::vector<int32_t> m_matGlass;
  std::vector<int32_t> m_matLeather;
  std::vector<int32_t> m_matLiquid;
  std::vector<int32_t> m_matMarble;
  std::vector<int32_t> m_matMetal;
  std::vector<int32_t> m_matOrganic;
  std::vector<int32_t> m_matPaint;
  std::vector<int32_t> m_matPaper;
  std::vector<int32_t> m_matPlastic;
  std::vector<int32_t> m_matRubber;
  std::vector<int32_t> m_matWood;
};

ICLSP_Randomizer* CreateCLSPRandomizer(const char *a_path)
{
  return new CLSP_Randomizer(a_path);
}


void CLSP_Randomizer::init(const char* a_path)
{
  const std::wstring wpath      = clsp_s2ws(a_path);

  const std::string pathToDB    = std::string(a_path) + "/test.db";
  const std::wstring pathToMTLS = wpath   + L"statex_00001.xml";

  m_db = clspDBOpen(pathToDB.c_str());

  if(m_db == nullptr)
  {
    std::cerr << "[CLSP_Randomizer]: can't open data base" << std::endl;
    return;
  }

  HRUtils::MergeLibraryIntoLibrary(wpath.c_str(), false, false,
                                   L"statex_00001.xml", &m_mergeInfo);

  // custom data and logic
  //

  // make material list without textures ... THIS IS TEMPORARY SOLUTION !!!!
  //

  m_materialsWithoutTextures.reserve(100); m_materialsWithoutTextures.resize(0);
  m_materialsAll.reserve(100);             m_materialsAll.resize(0);

 // std::wcout << L"Collection of all materials from the library in separate arrays." << std::endl;

  for(int32_t matId = m_mergeInfo.materialRange[0]; matId < m_mergeInfo.materialRange[1]; matId++)
  {
    bool haveTextures = false;
    bool exclude      = false;

    HRMaterialRef matRef; matRef.id = matId;

    hrMaterialOpen(matRef, HR_OPEN_READ_ONLY);
    auto node = hrMaterialParamNode(matRef);

    if(node.child(L"diffuse")     .child(L"color")      .child(L"texture") != nullptr) haveTextures = true;
    if(node.child(L"displacement").child(L"height_map") .child(L"texture") != nullptr) haveTextures = true;
    if(node.child(L"displacement").child(L"normal_map") .child(L"texture") != nullptr) haveTextures = true;

    // Metadata

    if(node.child(L"metadata") == nullptr)
      exclude = true;
  
    //auto nodeEmission = node.child(L"emission").child(L"color");
    //
    //if(nodeEmission != nullptr &&
    //   nodeEmission.attribute(L"val") != nullptr &&
    //   std::wstring(nodeEmission.attribute(L"val").value()) != L"0 0 0")
    //   exclude = true;


    // Add all targets tags to separate arrays 

    if (!exclude)
    {
      std::wstring targetName = node.child(L"metadata").attribute(L"target").as_string();

      mapTagsTargets[L"PCscreen"]               = &m_targPCscreen;
      mapTagsTargets[L"PCkeyboard"]             = &m_targPCkeyboard;
      mapTagsTargets[L"MacKeyboard"]            = &m_targMacKeyboard;
      mapTagsTargets[L"SmartphoneScreen"]       = &m_targSmartphoneScreen;
      mapTagsTargets[L"TabletScreen"]           = &m_targTabletScreen;
      mapTagsTargets[L"RulerStraight"]          = &m_targRulerStraight;
      mapTagsTargets[L"RulerAngular"]           = &m_targRulerAngular;
      mapTagsTargets[L"RulerProtractor"]        = &m_targRulerProtractor;
      mapTagsTargets[L"Notepad"]                = &m_targNotepad;
      mapTagsTargets[L"BoxPaperclips"]          = &m_targBoxPaperclips;
      mapTagsTargets[L"BoxStaples"]             = &m_targBoxStaples;
      mapTagsTargets[L"PictureHorizontal"]      = &m_targPictureHorizontal;
      mapTagsTargets[L"PictureVertical"]        = &m_targPictureVertical;
      mapTagsTargets[L"ClockCircle"]            = &m_targClockCircle;
      mapTagsTargets[L"ClockSquare"]            = &m_targClockSquare;
      mapTagsTargets[L"ClockRecrangular"]       = &m_targClockRecrangular;
      mapTagsTargets[L"GlossyMetal"]            = &m_targGlossyMetal;
      mapTagsTargets[L"GlossyMetalHole"]        = &m_targGlossyMetalHole;
      mapTagsTargets[L"GlossyPlasticDark"]      = &m_targGlossyPlasticDark;
      mapTagsTargets[L"GlossyPlasticHole"]      = &m_targGlossyPlasticHole;
      mapTagsTargets[L"Diffuse"]                = &m_targDiffuse;
      mapTagsTargets[L"GlassClear"]             = &m_targGlassClear;
      mapTagsTargets[L"Chrome"]                 = &m_targChrome;
      mapTagsTargets[L"CalendarOrPosterHoriz"]  = &m_targCalendarOrPosterHoriz;
      mapTagsTargets[L"Book"]                   = &m_targBook;
      mapTagsTargets[L"Whiteboard"]             = &m_targWhiteboard;
      mapTagsTargets[L"StorageBox"]             = &m_targStorageBox;
      mapTagsTargets[L"CalendarOrPosterVert"]   = &m_targCalendarOrPosterVert;
      mapTagsTargets[L"ChairLeg"]               = &m_targChairLeg;
      mapTagsTargets[L"CasePC"]                 = &m_targCasePC;
      mapTagsTargets[L"BlackPlastic"]           = &m_targBlackPlastic;
      mapTagsTargets[L"ChairFabric"]            = &m_targChairFabric;
      mapTagsTargets[L"GlossyPlasticLight"]     = &m_targGlossyPlasticLight;
      mapTagsTargets[L"Lampshade"]              = &m_targLampshade;

      for (int i = 1; i < targetList.size(); i++) // i = 0 skip because it is "Undefined".
      {
        if (targetName.find(targetList[i]) != std::wstring::npos)
        {
          mapTagsTargets[targetList[i]]->push_back(matId);
          #ifdef WIN32
          std::wcout << green << L"Found and add target in array: " << targetList[i] << white << std::endl;
          #else
         // std::wcout << L"Found and add target in array: " << targetList[i] << std::endl;
          #endif
        }
      }


      // Add all category tags to separate arrays 

      std::wstring cateroryName = node.child(L"metadata").attribute(L"category").as_string();
      //std::wcout << L"Category name: " << cateroryName << std::endl;

      mapTagsMaterials[L"Ceramic"]   = &m_matCeramic;
      mapTagsMaterials[L"Concrete"]  = &m_matConcrete;
      mapTagsMaterials[L"Emission"]  = &m_matEmission;
      mapTagsMaterials[L"Fabric"]    = &m_matFabric;
      mapTagsMaterials[L"Glass"]     = &m_matGlass;
      mapTagsMaterials[L"Leather"]   = &m_matLeather;
      mapTagsMaterials[L"Liquid"]    = &m_matLiquid;
      mapTagsMaterials[L"Marble"]    = &m_matMarble;
      mapTagsMaterials[L"Metal"]     = &m_matMetal;
      mapTagsMaterials[L"Organic"]   = &m_matOrganic;
      mapTagsMaterials[L"Paint"]     = &m_matPaint;
      mapTagsMaterials[L"Paper"]     = &m_matPaper;
      mapTagsMaterials[L"Plastic"]   = &m_matPlastic;
      mapTagsMaterials[L"Rubber"]    = &m_matRubber;
      mapTagsMaterials[L"Wood"]      = &m_matWood;

      for (int i = 0; i < categoryList.size(); i++)
      {
        if (cateroryName.find(categoryList[i]) != std::wstring::npos)
        {
          mapTagsMaterials[categoryList[i]]->push_back(matId);
          #ifdef WIN32
          std::wcout << green << L"Found and add category in array: " << categoryList[i] << white << std::endl;
          #else
      //    std::wcout << L"Found and add category in array: " << categoryList[i] << std::endl;
          #endif
        }
      }      
    }

    hrMaterialClose(matRef);

    if(!haveTextures && !exclude) m_materialsWithoutTextures.push_back(matId);
    if(!exclude) m_materialsAll.push_back(matId);
  }
  
  // add human materials now
  //
  const std::string pathMale   = std::string(a_path) + "/textures_human/male";
  const std::string pathFemale = std::string(a_path) + "/textures_human/female";
  
  m_materialsHumanMale.resize(0);   m_materialsHumanMale.reserve(100);
  m_materialsHumanFemale.resize(0); m_materialsHumanFemale.reserve(100);
  
  auto path1W = clsp_s2ws(pathMale);
  auto path2W = clsp_s2ws(pathFemale);

  auto texturesMale   = hr_fs::listfiles(path1W.c_str());
  auto texturesFemale = hr_fs::listfiles(path2W.c_str());

  for (auto texpath : texturesMale)
    m_materialsHumanMale.push_back(CreateDiffuseMaterialWithTexture(texpath).id);

  for (auto texpath : texturesFemale)
    m_materialsHumanFemale.push_back(CreateDiffuseMaterialWithTexture(texpath).id);

  
  
}

std::vector<std::string> CLSP_Randomizer::ListModelCategories()
{
  if(m_db == nullptr)
    return std::vector<std::string>();

  return clspList3DModelsCategories(m_db);
}

std::vector<CSLP_3DModel> CLSP_Randomizer::SelectModels_WhereFitIn(const char* condition, float a_boxSize[3])
{
  if(m_db == nullptr)
    return std::vector<CSLP_3DModel>();

  return clspSelect3DModels_Where_Fit_In(m_db, condition, a_boxSize);
}

static std::vector<int32_t> _MaterialList(const HRMeshInfo& a_meshInfo)
{
  std::vector<int32_t> res(a_meshInfo.matNamesListSize);
  for(size_t i=0;i<res.size(); i++)
    res[i] = a_meshInfo.batchesList[i].matId;
  return res;
}

static std::vector<int32_t> _RemapMaterials(const HRMeshInfo& a_meshInfo, std::vector<int32_t> newMatId)
{
  auto mindOriginal  = _MaterialList(a_meshInfo);
  std::vector<int32_t> remapList(mindOriginal.size()*2);
  for(size_t i=0;i<mindOriginal.size();i++)
  {
    remapList[i*2+0] = mindOriginal[i];
    remapList[i*2+1] = newMatId[i];
  }
  return remapList;
}

std::vector<int32_t> CLSP_Randomizer::SampleRemapList(const CSLP_3DModel& a_model, HRMeshRef a_mesh, const std::vector<float> rndArray)
{
  int32_t randomId        = 0;
  int32_t newRndMatId     = 0;
  auto meshInfo           = hrMeshGetInfo(a_mesh);
  const int32_t numSubMat = meshInfo.matNamesListSize;

  std::vector<int32_t> newSubMatId(numSubMat);


  for (int  i = 0; i < numSubMat; i++)
  {    
    std::vector<int32_t> newId;

    if(a_model.category == "cg_common")
    {
      randomId    = simplerandom::mapRndFloatToInt(rndArray[i], 0, int(m_materialsWithoutTextures.size()) - 1);
      newId.push_back(m_materialsWithoutTextures[randomId]);
    }
    else if(a_model.category == "human")
    {
      if(a_model.subcategory == "male")
      {
        randomId    = simplerandom::mapRndFloatToInt(rndArray[i], 0, int(m_materialsHumanMale.size()) - 1);
        newId.push_back(m_materialsHumanMale[randomId]);
      }
      else
      {
        randomId    = simplerandom::mapRndFloatToInt(rndArray[i], 0, int(m_materialsHumanFemale.size()) - 1);
        newId.push_back(m_materialsHumanFemale[randomId]);
      }
    }
    else // not common objects (primitives, budha and etc.) and human
    {
      const std::wstring matName = meshInfo.matNamesList[i];
    //  std::wcout << L"matName: " << matName.c_str() << std::endl;

      // Target tags      
      if (!(matName.find(L"Undefined") != std::wstring::npos)) // has target
      {
        for (int i = 0; i < targetList.size(); i++)
        {
          if (matName.find(targetList[i]) != std::wstring::npos)
          {
            #ifdef WIN32
            std::wcout << green << L"Found material: " << targetList[i] << white << std::endl;
            #else
       //     std::wcout << L"Found material: " << targetList[i] << std::endl;
            #endif
            const auto currTargArray = mapTagsTargets[targetList[i]];
            const int  sizeArray = currTargArray->size();

            if (sizeArray > 0)
            {
              randomId = simplerandom::mapRndFloatToInt(rndArray[i], 0, int(sizeArray - 1));   
              if (sizeArray == 1) 
                randomId = 0;
              newId.push_back((*currTargArray)[randomId]);
           //   std::wcout << L"Add new mat id: " << (*currTargArray)[randomId] << std::endl;
            }
            else
              HrPrint(HR_SEVERITY_ERROR, L"Current array mat empty! ");
          }
        }
      }
      else
      {
        // Materials tags
        for (int i = 0; i < categoryList.size(); i++)
        {
          if (matName.find(categoryList[i]) != std::wstring::npos)
          {
            #ifdef WIN32
            std::wcout << green << L"Found material: " << categoryList[i] << white << std::endl;
            #else
          //  std::wcout << L"Found material: " << categoryList[i] << std::endl;
            #endif
            
            const auto currMatArray = mapTagsMaterials[categoryList[i]];
            const int  sizeArray    = currMatArray->size();

            if (sizeArray > 0)
            {
              randomId = simplerandom::mapRndFloatToInt(rndArray[i], 0, int(sizeArray - 1));
              newId.push_back((*currMatArray)[randomId]);
         //     std::wcout << L"Add new mat id: " << (*currMatArray)[randomId] << std::endl;
            }
            else
              HrPrint(HR_SEVERITY_ERROR, L"Current array mat empty! ");
          }
        }
      }
    }

    if (newId.size() > 0)
    {
      randomId = simplerandom::mapRndFloatToInt(rndArray[i], 0, int(newId.size()) - 1);
      newRndMatId = newId[randomId];
    }
    else
      newRndMatId = 0;


    newSubMatId[i] = newRndMatId; // new random material from materials library
  }

  const auto remapList = _RemapMaterials(meshInfo, newSubMatId);   

  return remapList;
}
