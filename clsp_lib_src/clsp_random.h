//
// Created by frol on 4/26/19.
//

#ifndef CLSP_CLSP_RANDOM_H
#define CLSP_CLSP_RANDOM_H


namespace simplerandom
{
  struct uint2
  {
    unsigned int x;
    unsigned int y;
  };
  
  typedef struct RandomGenT
  {
    uint2 state;
    unsigned int maxNumbers;
    unsigned int lazy; // or dummy to have uint4 generator data.
    
  } RandomGen;
  
  
  static inline unsigned int NextState(RandomGen *gen)
  {
    const unsigned int x = (gen->state).x * 17 + (gen->state).y * 13123;
    (gen->state).x = (x << 13) ^ x;
    (gen->state).y ^= (x << 7);
    return x;
  }
  
  
  static inline RandomGen RandomGenInit(const int a_seed)
  {
    RandomGen gen;
    
    gen.state.x = (a_seed * (a_seed * a_seed * 15731 + 74323) + 871483);
    gen.state.y = (a_seed * (a_seed * a_seed * 13734 + 37828) + 234234);
    gen.lazy    = 0;
    
    for (int i = 0; i < (a_seed % 7); i++)
      NextState(&gen);
    
    return gen;
  }
  
  static inline float rndFloat(RandomGen *gen)
  {
    const unsigned int x   = NextState(gen);
    const unsigned int tmp = (x * (x * x * 15731 + 74323) + 871483);
    const float scale      = (1.0f / 4294967296.0f);
    return ((float) (tmp)) * scale;
  }
  
  static inline float rnd(RandomGen& gen, float s, float e)
  {
    const float t = rndFloat(&gen);
    return s + t*(e - s);
  }
  
  static inline unsigned int rand(RandomGen& gen)
  {
    return NextState(&gen);
  }
  
  static inline int mapRndFloatToInt(float a_val, int a, int b)
  {
    const float fa = (float)(a+0);
    const float fb = (float)(b+1);
    const float fR = fa + a_val * (fb - fa);
    
    const int res =  (int)(fR);
    
    if (res > b && b >= 0)
      return b;
    else
      return res;
  }
};


namespace qmc
{
  constexpr static int   QRNG_DIMENSIONS = 11;
  constexpr static int   QRNG_RESOLUTION = 31;
  constexpr static float INT_SCALE       = (1.0f / (float)0x80000001U);
  
  static inline float rndQmcSobolN(unsigned int pos, int dim, unsigned int *c_Table)
  {
    unsigned int result = 0;
    unsigned int data   = pos;
    
    for (int bit = 0; bit < QRNG_RESOLUTION; bit++, data >>= 1)
      if (data & 1) result ^= c_Table[bit + dim*QRNG_RESOLUTION];
    
    return (float)(result + 1) * INT_SCALE;
  }
};

void initQuasirandomGenerator(unsigned int table[qmc::QRNG_DIMENSIONS][qmc::QRNG_RESOLUTION]);


#endif //CLSP_CLSP_RANDOM_H
