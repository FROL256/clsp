#include "clsp.h"

#include <cstdio>
#include <cstring>
#include "../sqllite3/sqlite3.h"

#include <iostream>
#include <sstream>

#include <locale>                                         // wstring to string and back
#if (_POSIX_C_SOURCE >= 200112L || _XOPEN_SOURCE >= 600)  // wstring to string and back
#include <experimental/filesystem>                        // wstring to string and back
#endif

struct CSLP_Context
{
  sqlite3* db;
};



static int callback(void *NotUsed, int argc, char **argv, char **azColName)
{
  int i;
  for(i = 0; i<argc; i++)
    printf("%s = %s\n", azColName[i], argv[i] ? argv[i] : "NULL");
  printf("\n");
  return 0;
}


void CreateMaterialTables(sqlite3* a_pDB)
{
  if(a_pDB == nullptr)
  {
    std::cout << "CreateMaterialTables, nullptr database" << std::endl;
    return;
  }
  
  char* zErrMsg = nullptr;
  
  const char* sql = "CREATE TABLE `materials` (\n"
                    "\t`id`\tINTEGER NOT NULL PRIMARY KEY AUTOINCREMENT UNIQUE,\n"
                    "\t`name`\tTEXT NOT NULL UNIQUE,\n"
                    "\t`class`\tTEXT NOT NULL,\n"
                    "\t`target`\tTEXT\n"
                    ");";
  
  int rc = sqlite3_exec(a_pDB, sql, callback, 0, &zErrMsg);
  
  if( rc != SQLITE_OK )
  {
    fprintf(stderr, "CreateMaterialTables, SQL error: %s\n", zErrMsg);
    sqlite3_free(zErrMsg);
  }
  else
    fprintf(stdout, "CreateMaterialTables, table created successfully\n");
}

void Create3DModelsTables(sqlite3* a_pDB)
{
  if(a_pDB == nullptr)
  {
    std::cout << "Create3DModelsTables, nullptr database" << std::endl;
    return;
  }
  
  char* zErrMsg = nullptr;
  
  const char* sql = "CREATE TABLE `models` (\n"
                    "\t`id`\tINTEGER NOT NULL PRIMARY KEY AUTOINCREMENT UNIQUE,\n"
                    "\t`filepath`\tTEXT NOT NULL UNIQUE,\n"
                    "\t`bbox_size_x`\tREAL NOT NULL,\n"
                    "\t`bbox_size_y`\tREAL NOT NULL,\n"
                    "\t`bbox_size_z`\tREAL NOT NULL,\n"
                    "\t`mtl_names_list`\tTEXT,\n"
                    "\t`category`\tTEXT,\n"
                    "\t`subcategory`\tTEXT,\n"
                    "\t`style`\tTEXT, \n"
                    "\t`dataset_category`\tTEXT\n"
                    ");";
  
  int rc = sqlite3_exec(a_pDB, sql, callback, 0, &zErrMsg);
  
  if( rc != SQLITE_OK )
  {
    fprintf(stderr, "Create3DModelsTables, SQL error: %s\n", zErrMsg);
    sqlite3_free(zErrMsg);
  }
  else
    fprintf(stdout, "Create3DModelsTables, table created successfully\n");
}

void CreateEnvTables(sqlite3* a_pDB)
{
  if(a_pDB == nullptr)
  {
    std::cout << "CreateEnvTables, nullptr database" << std::endl;
    return;
  }
  
  char* zErrMsg = nullptr;
  
  const char* sql = "CREATE TABLE `envirs` (\n"
                    "\t`id`\tINTEGER NOT NULL PRIMARY KEY AUTOINCREMENT UNIQUE,\n"
                    "\t`filepath`\tTEXT NOT NULL UNIQUE,\n"
                    "\t`category`\tTEXT,\n"
                    "\t`daytime`\tTEXT,\n"
                    "\t`fileSource`\tTEXT \n"
                    ");";
  
  int rc = sqlite3_exec(a_pDB, sql, callback, 0, &zErrMsg);
  
  if( rc != SQLITE_OK )
  {
    fprintf(stderr, "CreateEnvTables, SQL error: %s\n", zErrMsg);
    sqlite3_free(zErrMsg);
  }
  else
    fprintf(stdout, "Create3DModelsTables, table created successfully\n");
}

void clspDebugTestCall(CSLP_Context* a_pDB, const char* a_data)
{
  CreateEnvTables(a_pDB->db);
}

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


CSLP_Context* clspDBOpen(const char* a_dbFileName)
{
  CSLP_Context* pContext = new CSLP_Context;
  
  char *zErrMsg = 0;
  int rc;
  
  rc = sqlite3_open(a_dbFileName, &pContext->db);
  
  if(rc)
  {
    fprintf(stderr, "Can't open database: %s\n", sqlite3_errmsg(pContext->db));
    // sqlite3_close(a_pDB->db); // do we need to close it ??
    delete pContext;
    return nullptr;
  }
  
  return pContext;
}

CSLP_Context* clspDBCreate(const char* a_dbFileName)
{
  if(remove(a_dbFileName) != 0)
  {
    std::cout << "clspDBCreate, can't delete old database file" << std::endl;
    //return nullptr;
  }
  
  auto* pContext = clspDBOpen(a_dbFileName);
  
  // (2) create tables
  //
  CreateMaterialTables(pContext->db);
  
  Create3DModelsTables(pContext->db);
  
  CreateEnvTables(pContext->db);
  
  return pContext;
}

void clspDBClose(CSLP_Context* a_pDB)
{
  if(a_pDB == nullptr)
    return;
  
  sqlite3_close(a_pDB->db);
  delete a_pDB;
}

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

#ifdef WIN32

#include <windows.h>

std::string clsp_ws2s(const std::wstring& s)
{
  int len;
  int slength = (int)s.length() + 1;
  len = WideCharToMultiByte(CP_ACP, 0, s.c_str(), slength, 0, 0, 0, 0);
  char* buf = new char[len];
  WideCharToMultiByte(CP_ACP, 0, s.c_str(), slength, buf, len, 0, 0);
  std::string r(buf);
  delete[] buf;
  return r;
}

std::wstring clsp_s2ws(const std::string& s)
{
  int len;
  int slength = (int)s.length() + 1;
  len = MultiByteToWideChar(CP_ACP, 0, s.c_str(), slength, 0, 0);
  wchar_t* buf = new wchar_t[len];
  MultiByteToWideChar(CP_ACP, 0, s.c_str(), slength, buf, len);
  std::wstring r(buf);
  delete[] buf;
  return r;
}
#else

std::wstring clsp_s2ws(const std::string& str)
{
  using convert_typeX = std::codecvt_utf8<wchar_t>;
  std::wstring_convert<convert_typeX, wchar_t> converterX;
  return converterX.from_bytes(str);
}

std::string clsp_ws2s(const std::wstring& wstr)
{
  using convert_typeX = std::codecvt_utf8<wchar_t>;
  std::wstring_convert<convert_typeX, wchar_t> converterX;
  return converterX.to_bytes(wstr);
}

#endif

void InsertMaterial(sqlite3* db, const CSLP_Material& a_material)
{
  char *zErrMsg      = nullptr;
  sqlite3_stmt *stmt = nullptr;
  const char *pzTest = nullptr;
  
  const char* sql = "INSERT INTO materials (name, class, target) values (?,?,?)";
  
  int rc = sqlite3_prepare(db, sql, strlen(sql), &stmt, &pzTest);
  
  if( rc != SQLITE_OK )
  {
    std::cout << "InsertMaterial, sqlite3_prepare failed: " << pzTest << std::endl;
    return;
  }
  
  // bind the value
  sqlite3_bind_text(stmt, 1, a_material.name.c_str(),   a_material.name.size(),   0);
  sqlite3_bind_text(stmt, 2, a_material.type.c_str(),   a_material.type.size(),   0);
  sqlite3_bind_text(stmt, 3, a_material.target.c_str(), a_material.target.size(), 0);
  
  // commit
  sqlite3_step(stmt);
  sqlite3_finalize(stmt);
}

void Insert3DModel(sqlite3* db, const CSLP_3DModel& a_model)
{
  char *zErrMsg      = nullptr;
  sqlite3_stmt *stmt = nullptr;
  const char *pzTest = nullptr;
  
  const char* sql = "INSERT INTO models (filepath, bbox_size_x, bbox_size_y, bbox_size_z, mtl_names_list, category, subcategory, style, dataset_category) " \
                    "values (?,?,?,?,?,?,?,?,?)";
  
  int rc = sqlite3_prepare(db, sql, strlen(sql), &stmt, &pzTest);
  
  if( rc != SQLITE_OK )
  {
    std::cout << "Insert3DModel, sqlite3_prepare failed: " << pzTest << std::endl;
    return;
  }
  
  // bind the value
  sqlite3_bind_text  (stmt, 1, a_model.filepath.c_str(), a_model.filepath.size(),   0);
  
  sqlite3_bind_double(stmt, 2, a_model.boxmax[0] - a_model.boxmin[0]);
  sqlite3_bind_double(stmt, 3, a_model.boxmax[1] - a_model.boxmin[1]);
  sqlite3_bind_double(stmt, 4, a_model.boxmax[2] - a_model.boxmin[2]);
  
  std::stringstream strOut;
  for(auto& x : a_model.matnames)
    strOut << x.c_str() << ";";
  
  const std::string allMaterials = strOut.str();
  
  sqlite3_bind_text(stmt, 5, allMaterials.c_str(), allMaterials.size(),                    0);
  sqlite3_bind_text(stmt, 6, a_model.category.c_str(),         a_model.category.size(),    0);
  sqlite3_bind_text(stmt, 7, a_model.subcategory.c_str(),      a_model.subcategory.size(), 0);
  sqlite3_bind_text(stmt, 8, a_model.style.c_str(),            a_model.style.size(),       0);
  sqlite3_bind_text(stmt, 9, a_model.dataset_category.c_str(), a_model.style.size(),       0);
  
  // commit
  sqlite3_step(stmt);
  sqlite3_finalize(stmt);
}

void InsertEnv(sqlite3* db, const CSLP_Envir& a_env)
{
  char *zErrMsg      = nullptr;
  sqlite3_stmt *stmt = nullptr;
  const char *pzTest = nullptr;
  
  const char* sql = "INSERT INTO envirs (filepath, category, daytime, filesource) values (?,?,?,?)";
  
  int rc = sqlite3_prepare(db, sql, strlen(sql), &stmt, &pzTest);
  
  if( rc != SQLITE_OK )
  {
    std::cout << "InsertEnv, sqlite3_prepare failed: " << pzTest << std::endl;
    return;
  }
  
  // bind the value
  sqlite3_bind_text(stmt, 1, a_env.path.c_str(),       a_env.path.size(),   0);
  sqlite3_bind_text(stmt, 2, a_env.category.c_str(),   a_env.category.size(),   0);
  sqlite3_bind_text(stmt, 3, a_env.daytime.c_str(),    a_env.daytime.size(), 0);
  sqlite3_bind_text(stmt, 4, a_env.fileSource.c_str(), a_env.fileSource.size(), 0);
  
  // commit
  sqlite3_step(stmt);
  sqlite3_finalize(stmt);
}


void clspInsert(CSLP_Context* a_ctx, const std::vector<CSLP_Material>& a_materials)
{
  if(a_ctx == nullptr)
  {
    std::cout << "clspInsert, nullptr context" << std::endl;
    return;
  }
  
  for(const auto& m : a_materials)
    InsertMaterial(a_ctx->db, m);
}

void clspInsert(CSLP_Context* a_ctx, const std::vector<CSLP_3DModel>& a_objects)
{
  if(a_ctx == nullptr)
  {
    std::cout << "clspInsert, nullptr context" << std::endl;
    return;
  }
  
  for(const auto& o : a_objects)
    Insert3DModel(a_ctx->db, o);
}


void clspInsert(CSLP_Context* a_ctx, const std::vector<CSLP_Envir>&  a_envs)
{
  if(a_ctx == nullptr)
  {
    std::cout << "clspInsert, nullptr context" << std::endl;
    return;
  }
  
  for(const auto& o : a_envs)
    InsertEnv(a_ctx->db, o);
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

static int materialCallback(void *data, int argc, char **argv, char **azColName)
{
  std::vector<CSLP_Material>* pRes = (std::vector<CSLP_Material>*)data;
  
  CSLP_Material mat;
  for(int i = 0; i<argc; i++)
  {
    const std::string attrName = azColName[i];
    const std::string attrVal  = argv[i] ? argv[i] : "NULL";
  
    if(attrName == "id")
      mat.dbId = atoi(attrVal.c_str());
    else if(attrName == "name")
      mat.name = attrVal;
    else if(attrName == "class")
      mat.type = attrVal;
    else if(attrName == "target")
      mat.target = attrVal;
  }
  
  pRes->push_back(mat);
  return 0;
}

std::vector<CSLP_Material> clspSelectMaterialsWhere(CSLP_Context* a_ctx, const char* a_sql)
{
  if(a_ctx == nullptr)
    return std::vector<CSLP_Material>();
  
  std::vector<CSLP_Material> res;
  res.reserve(1000);
  
  const std::string sql = std::string("SELECT * FROM materials WHERE ") + a_sql;
  char *zErrMsg = nullptr;
  
  int rc = sqlite3_exec(a_ctx->db, sql.c_str(), materialCallback, (void*)&res, &zErrMsg);
  
  if( rc != SQLITE_OK )
  {
    fprintf(stderr, "clspSelectMaterialsWhere, SQL error: %s\n", zErrMsg);
    sqlite3_free(zErrMsg);
  }
  
  return res;
}

std::vector<CSLP_3DModel> clspFitInBBox(const std::vector<CSLP_3DModel>& a_models, float a_boxSize[3])
{
  std::vector<CSLP_3DModel> filtered;
  filtered.reserve(a_models.size());
  
  const float sizeXB = a_boxSize[0];
  const float sizeYB = a_boxSize[1];
  const float sizeZB = a_boxSize[2];
  
  for(const auto& model : a_models)
  {
    const float sizeX = model.boxmax[0] - model.boxmin[0];
    const float sizeY = model.boxmax[1] - model.boxmin[1];
    const float sizeZ = model.boxmax[2] - model.boxmin[2];
    
    const bool fitX = (sizeX <= sizeXB);
    const bool fitY = (sizeY <= sizeYB);
    const bool fitZ = (sizeZ <= sizeZB);
    
    if(fitX && fitY && fitZ)
      filtered.push_back(model);
  }
  
  return filtered;
}


static int modelsCallback(void *data, int argc, char **argv, char **azColName)
{
  std::vector<CSLP_3DModel>* pRes = (std::vector<CSLP_3DModel>*)data;
  
  CSLP_3DModel model;
  for(int i = 0; i<argc; i++)
  {
    const std::string attrName = azColName[i];
    const std::string attrVal  = argv[i] ? argv[i] : "NULL";
    
    if(attrName == "id")
      model.dbId = atoi(attrVal.c_str());
    else if(attrName == "filepath")
      model.filepath = attrVal;
    else if(attrName == "category")
      model.category = attrVal;
    else if(attrName == "subcategory")
      model.subcategory = attrVal;
    else if(attrName == "style")
      model.style = attrVal;
    else if(attrName == "dataset_category")
      model.dataset_category = attrVal;
    else if (attrName == "bbox_size_x")
    {
      const float size = atof(attrVal.c_str());
      model.boxmin[0] = -0.5f*size;
      model.boxmax[0] = +0.5f*size;
    }
    else if (attrName == "bbox_size_y")
    {
      const float size = atof(attrVal.c_str());
      model.boxmin[1] = -0.5f*size;
      model.boxmax[1] = +0.5f*size;
    }
    else if (attrName == "bbox_size_z")
    {
      const float size = atof(attrVal.c_str());
      model.boxmin[2] = -0.5f*size;
      model.boxmax[2] = +0.5f*size;
    }
    else if (attrName == "mtl_names_list")
    {
      std::stringstream test(attrVal);
      std::string segment;
      while(std::getline(test, segment, ';')) // split single string to material names array
        model.matnames.push_back(segment);
    }
  }
  
  pRes->push_back(model);
  return 0;
}

std::vector<CSLP_3DModel> clspSelect3DModelsWhere(CSLP_Context* a_ctx, const char* a_sql)
{
  if(a_ctx == nullptr)
    return std::vector<CSLP_3DModel>();
  
  std::vector<CSLP_3DModel> res;
  res.reserve(1000);
  
  const std::string sql = std::string("SELECT * FROM models WHERE ") + a_sql;
  char *zErrMsg = nullptr;
  
  int rc = sqlite3_exec(a_ctx->db, sql.c_str(), modelsCallback, (void*)&res, &zErrMsg);
  
  if( rc != SQLITE_OK )
  {
    fprintf(stderr, "clspSelectMaterialsWhere, SQL error: %s\n", zErrMsg);
    sqlite3_free(zErrMsg);
  }
  
  return res;
}

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////


static int categoryCallback(void *data, int argc, char **argv, char **azColName)
{
  std::vector<CSLP_3DModel>* pRes = (std::vector<CSLP_3DModel>*)data;
  
  CSLP_3DModel model;
  for(int i = 0; i<argc; i++)
  {
    const std::string attrName = azColName[i];
    const std::string attrVal  = argv[i] ? argv[i] : "NULL";
    
    if(attrName == "category")
      model.category = attrVal;
  }
  
  pRes->push_back(model);
  return 0;
}

std::vector<std::string>  clspList3DModelsCategories(CSLP_Context *a_ctx)
{
  if(a_ctx == nullptr)
    return std::vector<std::string>();
  
  std::vector<CSLP_3DModel> res;
  res.reserve(1000);
  
  const std::string sql = std::string("SELECT DISTINCT category FROM models WHERE 1");
  char *zErrMsg = nullptr;
  
  int rc = sqlite3_exec(a_ctx->db, sql.c_str(), categoryCallback, (void*)&res, &zErrMsg);
  
  if( rc != SQLITE_OK )
  {
    fprintf(stderr, "clspSelectMaterialsWhere, SQL error: %s\n", zErrMsg);
    sqlite3_free(zErrMsg);
  }
  
  std::vector<std::string> cats;
  cats.reserve(100);
  for(const auto& m : res)
    cats.push_back(m.category);
  return cats;
}

std::vector<CSLP_3DModel> clspSelect3DModels_Where_Fit_In(CSLP_Context* a_ctx, const char* condition, float a_boxSize[3])
{
  std::stringstream strOut;
  strOut << " " << condition << " AND ";
  strOut << "(bbox_size_x <= " << a_boxSize[0] << ") AND (bbox_size_y <= " << a_boxSize[1] << ") AND (bbox_size_z <= " << a_boxSize[2] << ")";
  auto sqlTail = strOut.str();
  return clspSelect3DModelsWhere(a_ctx, sqlTail.c_str());
}
