//
// Created by frol on 23.04.19.
//

#ifndef CLSP_CLSP_HYDRA_H
#define CLSP_CLSP_HYDRA_H

#include "clsp.h"
#include "../../HydraAPI/hydra_api/HydraAPI.h"

std::vector<CSLP_Material> clspObtainMaterialsFromHydraSceneLibrary(const char* a_path);
std::vector<CSLP_3DModel>  clspObtainModelsFromFolder              (const char* a_path);
std::vector<CSLP_Envir>    clspListEnvirs                          (const char* a_path);

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


/**
\brief High Level API to CLSP Randomizer functionality
 *
 * Please note that it is RECOMMENDED TO CREATE 'CLSP_Randomizer' object AFTER you set up your scene!!!
 * This is due to 'CLSP_Randomizer' will import material library to merge it with your scene.
 *
 */

struct ICLSP_Randomizer
{
  ICLSP_Randomizer() {}
  virtual ~ICLSP_Randomizer(){}

  virtual std::vector<std::string>   ListModelCategories()                                              = 0;
  virtual std::vector<CSLP_3DModel>  SelectModels_WhereFitIn(const char* condition, float a_boxSize[3]) = 0;
  virtual std::vector<int32_t>       SampleRemapList(const CSLP_3DModel& a_model, HRMeshRef a_mesh, const std::vector<float> rndArray) = 0;

  static constexpr int numRndDigit = 64;
};

/**
\brief create actual implementation of 'ICLSP_Randomizer'
 *
 */
ICLSP_Randomizer* CreateCLSPRandomizer(const char *a_path);

#endif //CLSP_CLSP_HYDRA_H
