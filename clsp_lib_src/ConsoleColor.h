// ConsoleColor.h

#pragma once
#include <iostream>

#ifdef WIN32
#include <windows.h>

inline std::wostream& blue(std::wostream &s)
{
  HANDLE hStdout = GetStdHandle(STD_OUTPUT_HANDLE);
  SetConsoleTextAttribute(hStdout, FOREGROUND_BLUE
    | FOREGROUND_GREEN | FOREGROUND_INTENSITY);
  return s;
}

inline std::wostream& red(std::wostream &s)
{
  HANDLE hStdout = GetStdHandle(STD_OUTPUT_HANDLE);
  SetConsoleTextAttribute(hStdout,
    FOREGROUND_RED | FOREGROUND_INTENSITY);
  return s;
}

inline std::wostream& green(std::wostream &s)
{
  HANDLE hStdout = GetStdHandle(STD_OUTPUT_HANDLE);
  SetConsoleTextAttribute(hStdout,
    FOREGROUND_GREEN | FOREGROUND_INTENSITY);
  return s;
}

inline std::wostream& yellow(std::wostream &s)
{
  HANDLE hStdout = GetStdHandle(STD_OUTPUT_HANDLE);
  SetConsoleTextAttribute(hStdout,
    FOREGROUND_GREEN | FOREGROUND_RED | FOREGROUND_INTENSITY);
  return s;
}

inline std::wostream& white(std::wostream &s)
{
  HANDLE hStdout = GetStdHandle(STD_OUTPUT_HANDLE);
  SetConsoleTextAttribute(hStdout,
    FOREGROUND_RED | FOREGROUND_GREEN | FOREGROUND_BLUE);
  return s;
}

struct color {
  color(WORD attribute) :m_color(attribute) {};
  WORD m_color;
};

template <class _Elem, class _Traits>
std::basic_ostream<_Elem, _Traits>&
operator<<(std::basic_ostream<_Elem, _Traits>& i, color& c)
{
  HANDLE hStdout = GetStdHandle(STD_OUTPUT_HANDLE);
  SetConsoleTextAttribute(hStdout, c.m_color);
  return i;
}

// Copyleft Vincent Godin#pragma once
#endif