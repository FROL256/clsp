#include "clsp_lib_src/clsp.h"
#include "clsp_lib_src/clsp_hydra.h"
#include "clsp_lib_src/clsp_random.h"
#include "clsp_lib_src/ConsoleColor.h"

#include "utils.h"
#include "rand_model.h"

#include <iostream>
#include <fstream>
#include <sstream>
#include <iomanip>
#include <stdio.h>

#include <chrono>
#include <thread>

#include "../HydraAPI/hydra_api/LiteMath.h"
#include "../HydraAPI/hydra_api/HydraXMLHelpers.h"
#include "../HydraAPI/hydra_api/HydraXMLVerify.h"

static const float DEG_TO_RAD = 0.01745329251f; // float(3.14159265358979323846f) / 180.0f;

using LiteMath::float3;
using LiteMath::float4;
using LiteMath::float4x4;

void test_model_m2(ModelGenSettings a_settings)
{
  
  hrSceneLibraryOpen(L"temp", HR_WRITE_DISCARD);
  
  HRRenderRef    render; render.id = 0;
  
  HRMaterialRef matOfPlane     = hrMaterialCreate(L"PlaneMat");
  HRMaterialRef matOfTestModel = hrMaterialCreate(L"RedMaterial");
  
  HRTextureNodeRef texFloor    = hrTexture2DCreateFromFile(L"Textures_lib/10cm.jpg");
  
  HRTextureNodeRef texEnv, texBack;
  
  if(a_settings.lightingType == LIGHT_DEFAULT)
  {
    texEnv  = hrTexture2DCreateFromFile(L"Textures_lib/Envir/EnvMatEdit.hdr");
    texBack = hrTexture2DCreateFromFile(L"Textures_lib/Envir/Back.jpg");
  }
  else
  {
    texBack = hrTexture2DCreateFromFile(L"Textures_lib/Envir/Brooklyn_Bridge_Planks_2k.hdr");
    texEnv  = hrTexture2DCreateFromFile(L"Textures_lib/Envir/20_Subway_Lights_3k.hdr");
  }
  
  hrMaterialOpen(matOfPlane, HR_WRITE_DISCARD);
  {
    auto matNode = hrMaterialParamNode(matOfPlane);
    
    if(a_settings.floorType == FLOOR_GREY)
    {
      auto diff    = matNode.append_child(L"diffuse");
      diff.append_attribute(L"brdf_type").set_value(L"lambert");
      diff.append_child(L"color").append_attribute(L"val") = L"1.0 1.0 1.0";
      diff.append_attribute(L"tex_apply_mode").set_value(L"replace");
  
      auto texNode = hrTextureBind(texFloor, diff);
  
      texNode.append_attribute(L"matrix");
      
      const float s = 5.0f*a_settings.floorSizeInMeters;
      
      float samplerMatrix[16] = {s, 0, 0, 0,
                                 0, s, 0, 0,
                                 0, 0, 1, 0,
                                 0, 0, 0, 1};
  
      texNode.append_attribute(L"addressing_mode_u").set_value(L"wrap");
      texNode.append_attribute(L"addressing_mode_v").set_value(L"wrap");
      texNode.append_attribute(L"input_gamma").set_value(2.2f);
      texNode.append_attribute(L"input_alpha").set_value(L"rgb");
  
      HydraXMLHelpers::WriteMatrix4x4(texNode, L"matrix", samplerMatrix);
    }
    else if(a_settings.floorType == FLOOR_SHADOW_MATTE)
    {
      matNode.attribute(L"type") = L"shadow_catcher";
  
      auto opacity = matNode.append_child(L"opacity");
      opacity.append_child(L"skip_shadow").append_attribute(L"val").set_value(1);
  
      //auto back = matNode.append_child(L"back");
      //back.append_attribute(L"reflection") = 1;
  
      //auto texNode = hrTextureBind(texBack, back);
      //texNode.append_attribute(L"input_gamma") = 2.2f;
  
      //auto shadow = matNode.append_child(L"shadow");
      //shadow.append_child(L"color").append_attribute(L"val").set_value(L"0.0 0.0 0.5");
  
      VERIFY_XML(matNode);
    }
  }
  hrMaterialClose(matOfPlane);
  
  hrMaterialOpen(matOfTestModel, HR_WRITE_DISCARD);
  {
    auto matNode = hrMaterialParamNode(matOfTestModel);
    auto diff    = matNode.append_child(L"diffuse");
    
    diff.append_attribute(L"brdf_type").set_value(L"lambert");
    diff.append_child(L"color").append_attribute(L"val") = L"0.5 0.05 0.05";
  }
  hrMaterialClose(matOfTestModel);
  
  
  /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
  // Meshes
  /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
  
  HRMeshRef planeRef = HRMeshFromSimpleMesh(L"my_plane", CreatePlane(a_settings.floorSizeInMeters), matOfPlane.id);
  
  /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
  // Light
  /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
  
  HRLightRef sky = hrLightCreate(L"sky");
  
  //HRLightRef sun = hrLightCreate(L"sun");

  //hrLightOpen(sun, HR_WRITE_DISCARD);
  //{
  //  auto lightNode = hrLightParamNode(sun);

  //  lightNode.attribute(L"type").set_value(L"directional");
  //  lightNode.attribute(L"shape").set_value(L"point");
  //  lightNode.attribute(L"distribution").set_value(L"directional");

  //  auto sizeNode = lightNode.append_child(L"size");
  //  sizeNode.append_attribute(L"inner_radius").set_value(L"0.0");
  //  sizeNode.append_attribute(L"outer_radius").set_value(L"50.0");

  //  auto intensityNode = lightNode.append_child(L"intensity");

  //  intensityNode.append_child(L"color").append_attribute(L"val").set_value(L"1.0 0.85 0.64");
  //  intensityNode.append_child(L"multiplier").append_attribute(L"val").set_value(5.0);

  //  lightNode.append_child(L"shadow_softness").append_attribute(L"val").set_value(1.0f);
  //  lightNode.append_child(L"angle_radius").append_attribute(L"val").set_value(0.25f);

  //  VERIFY_XML(lightNode);
  //}
  //hrLightClose(sun);

  hrLightOpen(sky, HR_WRITE_DISCARD);
  {
    auto lightNode = hrLightParamNode(sky);
    
    lightNode.attribute(L"type").set_value(L"sky");
    lightNode.attribute(L"distribution").set_value(L"map");
    
    auto intensityNode = lightNode.append_child(L"intensity");
    
    intensityNode.append_child(L"color").append_attribute(L"val").set_value(L"1 1 1");
    intensityNode.append_child(L"multiplier").append_attribute(L"val").set_value(L"1.0");

    auto texNode = hrTextureBind(texEnv, intensityNode.child(L"color"));
  
    if(a_settings.lightingType != LIGHT_RAND_ENV)
    {
      auto back = lightNode.append_child(L"back");
      auto tex  = hrTextureBind(texBack, back);
  
      int w,h,bpp;
      hrTexture2DGetSize(texBack, &w, &h, &bpp);
      
      if(a_settings.lightingType == LIGHT_DEFAULT)
      {
        back.append_attribute(L"mode") = L"spherical";
        tex.append_attribute(L"input_gamma") = (bpp == 4) ? 2.2f : 1.0f;

        texNode.append_attribute(L"matrix");
        float samplerMatrix[16] = {1, 0, 0, 0.5, // { 1, 0, 0, offset,
                                   0, 1, 0, 0,
                                   0, 0, 1, 0,
                                   0, 0, 0, 1};
        
        texNode.append_attribute(L"addressing_mode_u").set_value(L"wrap");
        texNode.append_attribute(L"addressing_mode_v").set_value(L"wrap");        
        HydraXMLHelpers::WriteMatrix4x4(texNode, L"matrix", samplerMatrix);
      }
      else if (a_settings.lightingType == LIGHT_RAND_ENV_AND_BACK)
      {
        back.append_attribute(L"mode") = L"spherical";
        tex.append_attribute(L"input_gamma") = (bpp == 4) ? 2.2f : 1.0f;
      }
      else
      {
        back.append_attribute(L"mode")       = L"camera_mapped"; // or spherical
        tex.append_attribute(L"input_gamma") = (bpp == 4) ? 2.2f : 1.0f;
      }      
    }
    VERIFY_XML(lightNode);
  }
  hrLightClose(sky);
  
  
  ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
  ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
  
  // set camera
  //
  HRCameraRef camRef    = hrCameraCreate(L"mycamera");
  HRSceneInstRef scene  = hrSceneCreate(L"myscene");
  HRRenderRef renderRef = hrRenderCreate(L"HydraModern"); // opengl1
  
  hrRenderEnableDevice(renderRef, 0, true);
  
  ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
  ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
  
  ICLSP_Randomizer* pRandomizer = CreateCLSPRandomizer(".");
  
  float bboxSize[3] = {3,3,3};
  
  // Select models by some condition from database
  std::vector<CSLP_3DModel> models = pRandomizer->SelectModels_WhereFitIn(a_settings.sqlSuffix, bboxSize);
  simplerandom::RandomGen gen      = simplerandom::RandomGenInit(0); // or Sobol
  
  
  std::cout << "models num = " << models.size() << std::endl;
  
  ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
  ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
  const int numRenderImagesForModel = a_settings.numOfExamples;
  int prevNum = 1;
  
  for (int modelId = 0; modelId < models.size();)
  {      
    if (prevNum > numRenderImagesForModel)
    {
      modelId++;
      prevNum = 1;
    }
    
    std::vector<float> rndArray(ICLSP_Randomizer::numRndDigit);
    for (int i = 0; i < rndArray.size(); i++)
      rndArray[i] = simplerandom::rndFloat(&gen); // or Sobol
      
    std::wstring pathW  = clsp_s2ws(models[modelId].filepath);
    HRMeshRef testModel = hrMeshCreateFromFileDL(pathW.c_str());
    std::cout << "Model path: " << models[modelId].filepath.c_str() << std::endl;

    const auto remapList = pRandomizer->SampleRemapList(models[modelId], testModel, rndArray);    
    
    /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    // Camera
    /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    
    const float3 bBoxMax = { models[modelId].boxmax[0], models[modelId].boxmax[1], models[modelId].boxmax[2] };
    const float3 bBoxMin = { models[modelId].boxmin[0], models[modelId].boxmin[1], models[modelId].boxmin[2] };
    const float sizeBbox = LiteMath::length(bBoxMax - bBoxMin);
    
    std::wstring cameraPos = L"0 " + std::to_wstring(sizeBbox * 1.0f) + L" "+ std::to_wstring(sizeBbox * 1.4f);
    std::wstring targetPos = L"0 " + std::to_wstring(models[modelId].boxmax[1]) + L" 0";
    
    hrCameraOpen(camRef, HR_WRITE_DISCARD);
    {
      auto camNode = hrCameraParamNode(camRef);
      
      camNode.append_child(L"fov").text().set(L"45");
      camNode.append_child(L"nearClipPlane").text().set(L"0.01");
      camNode.append_child(L"farClipPlane").text().set(L"100.0");
      
      camNode.append_child(L"up").text().set(L"0 1 0");
      camNode.append_child(L"position").text().set(cameraPos.c_str());
      camNode.append_child(L"look_at").text().set(targetPos.c_str());
    }
    hrCameraClose(camRef);
    
    /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    // Create scene
    /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    
    hrSceneOpen(scene, HR_WRITE_DISCARD);
    {      
      const float scale = 1.0f;
      auto mtranslate = LiteMath::translate4x4(float3(0, -scale * models[modelId].boxmin[1], 0));
      auto mscale     = LiteMath::scale4x4(float3(scale, scale, scale));
      auto mres       = mul(mtranslate, mscale);
      LiteMath::float4x4 mRot = LiteMath::rotate4x4Y(30.0f * DEG_TO_RAD);
      mres = mul(mtranslate, mRot);
      hrMeshInstance(scene, testModel, mres.L(), remapList.data(), remapList.size());      
      
      mtranslate.identity();      
      mRot.identity();
      mscale.identity();
      mscale = LiteMath::scale4x4(float3(scale, scale, -scale));
      mres = mul(mtranslate, mscale);
      hrMeshInstance(scene, planeRef, mres.L());
      
      mtranslate.identity();
      hrLightInstance(scene, sky, mtranslate.L());

      //Sun
      //LiteMath::float4x4 mTranslate;
      //LiteMath::float4x4 mRes;
      //LiteMath::float4x4 mRot2;

      //mTranslate.identity();
      //mRes.identity();
      //mRot.identity();
      //mRot2.identity();

      //mTranslate = translate4x4(float3(10.0f, 50.0f, -10.0f));
      //mRot = LiteMath::rotate_X_4x4(-45.0f*DEG_TO_RAD);
      //mRot2 = LiteMath::rotate_Z_4x4(-30.f*DEG_TO_RAD);
      //mRes = mul(mRot2, mRot);
      //mRes = mul(mTranslate, mRes);

      //hrLightInstance(scene, sun, mRes.L());

    }
    hrSceneClose(scene);
    
    /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    // Render settings
    /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    
    hrRenderOpen(renderRef, HR_WRITE_DISCARD);
    {
      pugi::xml_node node = hrRenderParamNode(renderRef);
      
      node.append_child(L"width").text()            = 512;
      node.append_child(L"height").text()           = 512;
      
      node.append_child(L"method_primary").text()   = L"pathtracing";
      node.append_child(L"method_secondary").text() = L"pathtracing";
      node.append_child(L"method_tertiary").text()  = L"pathtracing";
      node.append_child(L"method_caustic").text()   = L"none";
      
      //node.append_child(L"qmc_variant").text()      = 7;
      node.append_child(L"offline_pt").text()     = 1;
      
      node.append_child(L"trace_depth").text()      = 6;
      node.append_child(L"diff_trace_depth").text() = 3;
      node.append_child(L"maxRaysPerPixel").text()  = a_settings.spp;
    }
    hrRenderClose(renderRef);
    
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    
    hrFlush(scene, render, camRef);
    
    while (true)
    {
      std::this_thread::sleep_for(std::chrono::milliseconds(500));
      
      HRRenderUpdateInfo info = hrRenderHaveUpdate(render);
      
      if (info.haveUpdateFB)
      {
        auto pres = std::cout.precision(2);
        std::cout << "rendering progress = " << info.progress << "% \r";
        std::cout.precision(pres);
      }
      
      if (info.finalUpdate)
        break;
    }
    
    std::wstringstream namestream;
    namestream << std::fixed << a_settings.imagePath << std::setfill(L"0"[0]) << std::setw(3) << models[modelId].dbId << "_" << prevNum << L".png";
    const std::wstring imagePathW = namestream.str();
    
    //const std::wstring imagePathW = clsp_s2ws(a_imagePath);
    hrRenderSaveFrameBufferLDR(render, imagePathW.c_str());

    prevNum++;

    // Info file text
    std::wstringstream infoNameStream;
    infoNameStream << std::fixed << a_settings.imagePath << std::setfill(L"0"[0]) << std::setw(3) << models[modelId].dbId << L"_info.txt";
    const std::wstring infoFileName = infoNameStream.str();
  
    //const float width   = bBoxMax.x - bBoxMin.x;
    //const float length  = bBoxMax.y - bBoxMin.y;
    //const float height  = bBoxMax.z - bBoxMin.z;
    //std::ofstream infoFile(infoFileName);
    //infoFile << "model ID: \t"      << models[modelId].dbId << std::endl;
    //infoFile << "Filename model: " << models[modelId].filepath.c_str() << std::endl;
    //infoFile << "Width: \t\t" << width << std::endl;
    //infoFile << "Length: \t" << length << std::endl;
    //infoFile << "Height: \t" << height << std::endl;

  }
}