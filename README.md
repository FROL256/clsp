# CLSP

This is a 3D model generator project for sampling 3D scenes and training CNN. It consists of model data base and various generators/scene samplers.

![](image.png)

# Build Generator

1) git clone https://github.com/Ray-Tracing-Systems/HydraAPI
2) git clone https://github.com/Ray-Tracing-Systems/HydraCore
3) (optional) git clone https://gitlab.com/raytracingsystems/3dsmaxplugin
4) git clone https://gitlab.com/FROL256/clsp.git (this repository)

5) Build HydraAPI and HydraCore (using Cmake under Linux, using providewd VS projects under windows)
6) (optional) Builds 3ds max plugins
7) Build CLSP  (using Cmake under Linux, using providewd VS project under windows)

# Using generator

1) Initialize randomizer:
```cpp
ICLSP_Randomizer* pRandomizer = CreateCLSPRandomizer(".")
```

2) Select models from database using SQL command:
```cpp
std::string sqlSuffix = "category == 'Table' or category == 'Chair'"; // suffix of you SQL command 
float bboxSize[3] = {3,3,3};        // allowed bounding box of 3D model in meters, if don't care set to large values, like 100,100,100
std::vector<CSLP_3DModel> models = pRandomizer->SelectModels_WhereFitIn(sqlSuffix, bboxSize);
```
You can test you SQL and see models list using this program for example https://sqlitebrowser.org/
The database file is stored in 'database/test.db'.

![](sqlexample.png)

3) Put one of selected 3D models in scene (or all of them):
```cpp
std::wstring pathW  = clsp_s2ws(models[modelId].filepath);
HRMeshRef testModel = hrMeshCreateFromFileDL(pathW.c_str());
```
4) Generate your random numbers. Be sure that for same input random numbers you will always get same output 3D model.
```cpp
std::vector<float> rndArray(ICLSP_Randomizer::numRndDigit);
for (int i = 0; i < rndArray.size(); i++)
  rndArray[i] = hr_prng::rndFloat(&gen); // use hr_qmc::rndFloat(...) for Sobol sequence
```

5) Sample material remap list for single 3D model and add the model to scene (make an instance):
```cpp
const auto remapList = pRandomizer->SampleRemapList(models[modelId], testModel, rndArray);
hrMeshInstance(scene, testModel, transformMatrix.L(), remapList.data(), remapList.size());
```

6) Add other desired 3D models and lighting to you scene using HydraAPI.

# Acknowledgment

This project is supported by RFBR 18-31-20032 mol_a_ved.	
