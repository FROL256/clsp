#include "clsp_lib_src/clsp.h"
#include "clsp_lib_src/clsp_hydra.h"
#include "clsp_lib_src/clsp_random.h"
#include "clsp_lib_src/ConsoleColor.h"

#include "utils.h"

#include <iostream>
#include <fstream>
#include <sstream>
#include <iomanip>
#include <stdio.h>

#include <chrono>
#include <thread>

#include "../HydraAPI/hydra_api/LiteMath.h"
#include "../HydraAPI/hydra_api/HydraXMLHelpers.h"
#include "../HydraAPI/hydra_api/HydraXMLVerify.h"

static const float DEG_TO_RAD = 0.01745329251f; // float(3.14159265358979323846f) / 180.0f;

using LiteMath::float3;
using LiteMath::float4;
using LiteMath::float4x4;

static unsigned int g_sobolTable[qmc::QRNG_DIMENSIONS][qmc::QRNG_RESOLUTION];


// crate geometry
//

struct MyScene
{
  HRLightRef light;
  HRMeshRef  plane;
  
  constexpr static int OBJ_NUM = 3;
  HRMeshRef    objects    [OBJ_NUM];
  CSLP_3DModel objectsCLSP[OBJ_NUM];
};

MyScene make_test_scene_data(HRSceneInstRef scene, HRCameraRef camRef)
{
  HRMeshRef planeRef = HRMeshFromSimpleMesh(L"my_plane",  CreatePlane(20.0f), 0);
  
  HRLightRef directLight = hrLightCreate(L"my_direct_light");
  
  hrLightOpen(directLight, HR_WRITE_DISCARD);
  {
    pugi::xml_node lightNode = hrLightParamNode(directLight);
    
    lightNode.attribute(L"type").set_value(L"directional");
    lightNode.attribute(L"shape").set_value(L"point");
    
    // lightNode.append_child(L"direction").text() = L"-0.340738 -0.766534 -0.544356"; // will be recalculated and overrided by light instance matrix
    
    pugi::xml_node sizeNode = lightNode.append_child(L"size");
    
    sizeNode.append_child(L"inner_radius").text().set(L"15.0");
    sizeNode.append_child(L"outer_radius").text().set(L"30.0");
    
    pugi::xml_node intensityNode = lightNode.append_child(L"intensity");
    
    intensityNode.append_child(L"color").text().set(L"1 1 1");
    intensityNode.append_child(L"multiplier").text().set(L"2.0");
  }
  hrLightClose(directLight);
  
  MyScene res;     // #TODO: get models path from DB
  res.light      = directLight;
  res.plane      = planeRef;
  res.objects[0] = hrMeshCreateFromFileDL(L"/home/frol/PROG/CLSP_gitlab/database/models/various/lucy.vsgfc");
  res.objects[1] = hrMeshCreateFromFileDL(L"/home/frol/PROG/CLSP_gitlab/database/models/various/pyramid.vsgfc");
  res.objects[2] = hrMeshCreateFromFileDL(L"/home/frol/PROG/CLSP_gitlab/database/models/various/teapot.vsgfc");
  
  return res;
}

std::vector<int32_t> MaterialList(const HRMeshInfo& a_meshInfo)
{
  std::vector<int32_t> res(a_meshInfo.matNamesListSize);
  for(size_t i=0;i<res.size(); i++)
    res[i] = a_meshInfo.batchesList[i].matId;
  return res;
}

std::vector<int32_t> RemapMaterials(const HRMeshInfo& a_meshInfo, const int   matId)
{
  auto mindOriginal  = MaterialList(a_meshInfo);
  std::vector<int32_t> remapList(mindOriginal.size()*2);
  for(size_t i=0;i<mindOriginal.size();i++)
  {
    remapList[i*2+0] = mindOriginal[i];
    remapList[i*2+1] = matId;
  }
  return remapList;
}

using LiteMath::mul;

int sample_scene(MyScene a_scn, HRSceneInstRef scene, HRCameraRef camRef, int a_currSamplePos)
{
  constexpr int SAMPLE_NUM = 100;
  const float DEG_TO_RAD = float(3.14159265358979323846f) / 180.0f;
  
  simplerandom::RandomGen gen = simplerandom::RandomGenInit(777);
  
  // now finally generate scene
  //
  hrSceneOpen(scene, HR_WRITE_DISCARD);
  {
    float4x4 mtranslate, mscale, mrot, mres;
    
    // plane
    //
    mtranslate = LiteMath::translate4x4(float3(0.0f, 0.0f, 0.0f));
    hrMeshInstance(scene, a_scn.plane, mtranslate.L());
    
    // meshes
    //
    
    for (int i = 0; i <= SAMPLE_NUM; i++)
    {
      const float xCoord = -20 + 40*qmc::rndQmcSobolN(0               + i, 0, &g_sobolTable[0][0]);
      const float yCoord = -20 + 40*qmc::rndQmcSobolN(0               + i, 1, &g_sobolTable[0][0]);
      
      const float objIdf =          qmc::rndQmcSobolN(0               + i, 2, &g_sobolTable[0][0]);
      const float matIdf =          qmc::rndQmcSobolN(a_currSamplePos + i, 3, &g_sobolTable[0][0]);
      const float rotYf  =          qmc::rndQmcSobolN(a_currSamplePos + i, 4, &g_sobolTable[0][0]);
      
      const int   objId  = simplerandom::mapRndFloatToInt(objIdf, 0, MyScene::OBJ_NUM-1);
      const int   matId  = simplerandom::mapRndFloatToInt(matIdf, 1, 3);          // #TODO: get this from DB
      
      auto info      = hrMeshGetInfo(a_scn.objects[objId]);
      auto remapList = RemapMaterials(info, matId);
      
      const float rotAngle = rotYf*DEG_TO_RAD*360.0f;
      mrot = LiteMath::rotate4x4Y(rotAngle);
      
      if(objId == 0)                                                              // it seems lucy bounding box is broken ...
      {
        mtranslate = LiteMath::translate4x4(float3(xCoord, 0.05f, yCoord));  // it seems lucy bounding box is broken ...
        mscale     = LiteMath::scale4x4(float3(0.5f, 0.5f, 0.5f));
        mres       = mul(mtranslate, mul(mscale, mrot));
        hrMeshInstance(scene, a_scn.objects[objId], mres.L(), remapList.data(), remapList.size());
      }
      else
      {
        const float sizeY = 0.5f*(info.boxMax[1] - info.boxMin[1]);
        
        mscale     = LiteMath::scale4x4(float3(50.0f, 50.0f, 50.0f));
        mtranslate = LiteMath::translate4x4(float3(xCoord, sizeY, yCoord)); // put 3D model on the floor
        mres       = mul(mtranslate,  mul(mscale, mrot));
        hrMeshInstance(scene, a_scn.objects[objId], mres.L(), remapList.data(), remapList.size());
      }
      
    }
    
    // direct light
    //
    mtranslate = LiteMath::translate4x4(float3(0.0f, 100.0f, 0.0f));
    hrLightInstance(scene, a_scn.light, mtranslate.L());
  }
  hrSceneClose(scene);
  
  // set camera
  //
  hrCameraOpen(camRef, HR_WRITE_DISCARD);
  {
    auto camNode = hrCameraParamNode(camRef);
    
    camNode.append_child(L"fov").text().set(L"45");
    camNode.append_child(L"nearClipPlane").text().set(L"0.01");
    camNode.append_child(L"farClipPlane").text().set(L"100.0");
    
    camNode.append_child(L"up").text().set(L"0 1 0");
    camNode.append_child(L"position").text().set(L"-5 8 30");
    camNode.append_child(L"look_at").text().set(L"0.1 0 0.1");
  }
  hrCameraClose(camRef);
  
  return a_currSamplePos + SAMPLE_NUM;
}

#ifndef WIN32
void clean_up(const char* folder)
{
  auto files = hr_listfiles(folder);
  
  for(auto file : files)
  {
    if(file.find("statex_00001.xml") == std::string::npos)
    {
      if(file.find("statex_") != std::string::npos ||
         file.find("change_") != std::string::npos)
      {
        remove(file.c_str());
      }
    }
  }
}

#else
#include <windows.h>

void clean_up(const wchar_t* folder)
{
  auto files = hr_listfiles(folder, true);

  for (auto file : files)
  {
    if (file.find(L"statex_00001.xml") == std::string::npos)
    {
      if (file.find(L"statex_") != std::string::npos ||
          file.find(L"change_") != std::string::npos)
      {
        DeleteFileW(file.c_str());
      }
    }
  }
}

#endif


//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

MyScene make_test_scene_data_db(HRSceneInstRef scene, HRCameraRef camRef)
{
  HRMeshRef planeRef = HRMeshFromSimpleMesh(L"my_plane",  CreatePlane(20.0f), 0);
  
  HRLightRef directLight = hrLightCreate(L"my_direct_light");
  
  hrLightOpen(directLight, HR_WRITE_DISCARD);
  {
    pugi::xml_node lightNode = hrLightParamNode(directLight);
    
    lightNode.attribute(L"type").set_value(L"directional");
    lightNode.attribute(L"shape").set_value(L"point");
    
    // lightNode.append_child(L"direction").text() = L"-0.340738 -0.766534 -0.544356"; // will be recalculated and overrided by light instance matrix
    
    pugi::xml_node sizeNode = lightNode.append_child(L"size");
    
    sizeNode.append_child(L"inner_radius").text().set(L"15.0");
    sizeNode.append_child(L"outer_radius").text().set(L"30.0");
    
    pugi::xml_node intensityNode = lightNode.append_child(L"intensity");
    
    intensityNode.append_child(L"color").text().set(L"1 1 1");
    intensityNode.append_child(L"multiplier").text().set(L"2.0");
  }
  hrLightClose(directLight);
  
  ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////
  ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////
  
  CSLP_Context* db = clspDBOpen("test.db"); //#NOTE: assume your working directoty is "CLSP/database"
  
  if(db == nullptr)
  {
    std::cout << "[ERROR]: can't open database" << std::endl;
    return MyScene();
  }
  
  auto models = clspSelect3DModelsWhere(db, " category = 'various'");
  //auto models = clspSelect3DModelsWhere(db, " 1 ");
  
  if (models.size() == 0)
  {
    std::cout << "[ERROR]: no models " << std::endl;
    return MyScene();
  }
  
  for(auto model : models)
    std::cout << model.filepath << std::endl;
  
  auto wpath1 = clsp_s2ws(models[0].filepath);
  auto wpath2 = clsp_s2ws(models[1].filepath);
  auto wpath3 = clsp_s2ws(models[2].filepath);
  
  MyScene res;  // #TODO: get models path from DB
  res.light      = directLight;
  res.plane      = planeRef;
  res.objects[0] = hrMeshCreateFromFileDL(wpath1.c_str());
  res.objects[1] = hrMeshCreateFromFileDL(wpath2.c_str());
  res.objects[2] = hrMeshCreateFromFileDL(wpath3.c_str());
  
  //res.objectsCLSP[0] = models[0];
  //res.objectsCLSP[1] = models[1];
  //res.objectsCLSP[2] = models[2];
  
  return res;
}

int sample_scene_db(MyScene a_scn, HRSceneInstRef scene, HRCameraRef camRef, int a_currSamplePos)
{
  constexpr int SAMPLE_NUM = 100;
  const float DEG_TO_RAD = float(3.14159265358979323846f) / 180.0f;
  
  simplerandom::RandomGen gen = simplerandom::RandomGenInit(777);
  
  // now finally generate scene
  //
  hrSceneOpen(scene, HR_WRITE_DISCARD);
  {
    float4x4 mtranslate, mscale, mrot, mres;
    
    // plane
    //
    mtranslate = LiteMath::translate4x4(float3(0.0f, 0.0f, 0.0f));
    hrMeshInstance(scene, a_scn.plane, mtranslate.L());
    
    // meshes
    //
    for (int i = 0; i <= SAMPLE_NUM; i++)
    {
      const float xCoord = -20 + 40*qmc::rndQmcSobolN(0               + i, 0, &g_sobolTable[0][0]);
      const float yCoord = -20 + 40*qmc::rndQmcSobolN(0               + i, 1, &g_sobolTable[0][0]);
      
      const float objIdf =          qmc::rndQmcSobolN(0               + i, 2, &g_sobolTable[0][0]);
      const float matIdf =          qmc::rndQmcSobolN(a_currSamplePos + i, 3, &g_sobolTable[0][0]);
      const float rotYf  =          qmc::rndQmcSobolN(a_currSamplePos + i, 4, &g_sobolTable[0][0]);
      
      const int   objId  = simplerandom::mapRndFloatToInt(objIdf, 0, MyScene::OBJ_NUM-1);
      const int   matId  = simplerandom::mapRndFloatToInt(matIdf, 1, 3);          // #TODO: get this from DB
      
      auto info      = hrMeshGetInfo(a_scn.objects[objId]);
      auto remapList = RemapMaterials(info, matId);
      
      const float rotAngle = rotYf*DEG_TO_RAD*360.0f;
      mrot = LiteMath::rotate4x4Y(rotAngle);
      
      const float sizeY = 0.5f*(info.boxMax[1] - info.boxMin[1]);
      
      mscale     = LiteMath::scale4x4(float3(1.0f/sizeY, 1.0f/sizeY, 1.0f/sizeY));  // just an exampe how to ignore scale
      mtranslate = LiteMath::translate4x4(float3(xCoord, sizeY, yCoord));              // put 3D model on the floor
      mres       = mul(mtranslate,  mul(mscale, mrot));
      hrMeshInstance(scene, a_scn.objects[objId], mres.L(), remapList.data(), remapList.size());
    }
    
    // direct light
    //
    mtranslate = LiteMath::translate4x4(float3(0.0f, 100.0f, 0.0f));
    hrLightInstance(scene, a_scn.light, mtranslate.L());
  }
  hrSceneClose(scene);
  
  // set camera
  //
  hrCameraOpen(camRef, HR_WRITE_DISCARD);
  {
    auto camNode = hrCameraParamNode(camRef);
    
    camNode.append_child(L"fov").text().set(L"45");
    camNode.append_child(L"nearClipPlane").text().set(L"0.01");
    camNode.append_child(L"farClipPlane").text().set(L"100.0");
    
    camNode.append_child(L"up").text().set(L"0 1 0");
    camNode.append_child(L"position").text().set(L"-5 8 30");
    camNode.append_child(L"look_at").text().set(L"0.1 0 0.1");
  }
  hrCameraClose(camRef);
  
  return a_currSamplePos + SAMPLE_NUM;
}

void render_test_scene_db()
{
  // don't forget this please
  //
  initQuasirandomGenerator(g_sobolTable); // pat sable on the skin
  
  // open existing scene library
  //
  hrSceneLibraryOpen(L"./statex_00001.xml", HR_OPEN_EXISTING);
  
  HRRenderRef    render; render.id = 0;
  HRSceneInstRef scene;  scene.id  = 0;
  HRCameraRef   camRef;  camRef.id = 0;
  
  // create your custom scene
  //
  auto addedData = make_test_scene_data_db(scene, camRef);   // call this only once, add your depth buffer reconstruction and other
  
  // now render several random frames
  //
  int qmcPos = 0;
  for(int sceneId = 0; sceneId < 10; sceneId++)
  {
    qmcPos = sample_scene_db(addedData, scene, camRef, qmcPos); // may call several times to render several scenes
    
    hrFlush(scene, render, camRef);
    
    while (true)
    {
      std::this_thread::sleep_for(std::chrono::milliseconds(500));
      
      HRRenderUpdateInfo info = hrRenderHaveUpdate(render);
      
      if (info.haveUpdateFB)
      {
        auto pres = std::cout.precision(2);
        std::cout << "rendering progress = " << info.progress << "% \r";
        std::cout.precision(pres);
      }
      
      if (info.finalUpdate)
        break;
    }
    
    std::wstringstream namestream;
    namestream  << std::fixed << L"outimages/z_out_" << std::setfill(L"0"[0]) << std::setw(2) << sceneId << L".png";
    const std::wstring fileName = namestream.str();
    
    hrRenderSaveFrameBufferLDR(render, fileName.c_str());
  }

#ifndef WIN32
  clean_up(".");
#else
  clean_up(L".");
#endif
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
void test_model(const char* a_path, const char* a_imagePath)
{
  // don't forget this please
  //
  hrSceneLibraryOpen(L"temp", HR_WRITE_DISCARD);
  
  HRRenderRef    render; render.id = 0;
  
  HRMaterialRef matOfPlane     = hrMaterialCreate(L"PlaneMat");
  HRMaterialRef matOfTestModel = hrMaterialCreate(L"RedMaterial");
  
  hrMaterialOpen(matOfPlane, HR_WRITE_DISCARD);
  {
    auto matNode = hrMaterialParamNode(matOfPlane);
    auto diff    = matNode.append_child(L"diffuse");
    
    diff.append_attribute(L"brdf_type").set_value(L"lambert");
    diff.append_child(L"color").append_attribute(L"val") = L"0.25 0.25 0.25";
  }
  hrMaterialClose(matOfPlane);
  
  hrMaterialOpen(matOfTestModel, HR_WRITE_DISCARD);
  {
    auto matNode = hrMaterialParamNode(matOfTestModel);
    auto diff    = matNode.append_child(L"diffuse");
    
    diff.append_attribute(L"brdf_type").set_value(L"lambert");
    diff.append_child(L"color").append_attribute(L"val") = L"0.5 0.05 0.05";
  }
  hrMaterialClose(matOfTestModel);
  
  ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
  
  HRMeshRef planeRef = HRMeshFromSimpleMesh(L"my_plane", CreatePlane(20.0f), matOfPlane.id);
  
  HRLightRef directLight = hrLightCreate(L"my_direct_light");
  
  hrLightOpen(directLight, HR_WRITE_DISCARD);
  {
    pugi::xml_node lightNode = hrLightParamNode(directLight);
    
    lightNode.attribute(L"type").set_value(L"directional");
    lightNode.attribute(L"shape").set_value(L"point");
    
    // lightNode.append_child(L"direction").text() = L"-0.340738 -0.766534 -0.544356"; // will be recalculated and overrided by light instance matrix
    
    pugi::xml_node sizeNode = lightNode.append_child(L"size");
    
    sizeNode.append_child(L"inner_radius").text().set(L"15.0");
    sizeNode.append_child(L"outer_radius").text().set(L"30.0");
    
    pugi::xml_node intensityNode = lightNode.append_child(L"intensity");
    
    intensityNode.append_child(L"color").text().set(L"1 1 1");
    intensityNode.append_child(L"multiplier").text().set(L"2.0");
  }
  hrLightClose(directLight);
  
  ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
  
  
  //HRMeshRef testModel = hrMeshCreateFromFileDL(L"models/stationery/scotch_3dddru_0001.vsgfc");
  //HRMeshRef testModel = hrMeshCreateFromFileDL(L"models/cg_common/dragon.vsgfc");
  //HRMeshRef testModel = hrMeshCreateFromFileDL(L"models/figures/torus.vsgfc");
  
  std::wstring pathW  = clsp_s2ws(a_path);
  HRMeshRef testModel = hrMeshCreateFromFileDL(pathW.c_str());
  
  const float scale = 10.0f;
  
  auto info      = hrMeshGetInfo(testModel);
  auto remapList = RemapMaterials(info, matOfTestModel.id);
  
  ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
  
  // set camera
  //
  HRCameraRef camRef    = hrCameraCreate(L"mycamera");
  HRSceneInstRef scene  = hrSceneCreate(L"myscene");
  HRRenderRef renderRef = hrRenderCreate(L"HydraModern"); // opengl1
  
  hrRenderEnableDevice(renderRef, 0, true);
  
  hrCameraOpen(camRef, HR_WRITE_DISCARD);
  {
    auto camNode = hrCameraParamNode(camRef);
    
    
    camNode.append_child(L"fov").text().set(L"45");
    camNode.append_child(L"nearClipPlane").text().set(L"0.01");
    camNode.append_child(L"farClipPlane").text().set(L"100.0");
    
    camNode.append_child(L"up").text().set(L"0 1 0");
    camNode.append_child(L"position").text().set(L"-5 8 30");
    camNode.append_child(L"look_at").text().set(L"0.1 0 0.1");
  }
  hrCameraClose(camRef);
  
  hrSceneOpen(scene, HR_WRITE_DISCARD);
  {
    LiteMath::float4x4 identityMatrix;
    hrMeshInstance(scene, planeRef, identityMatrix.L());
    
    auto mtranslate = LiteMath::translate4x4(float3(0, -scale*info.boxMin[1], 0));  // it seems lucy bounding box is broken ...
    auto mscale     = LiteMath::scale4x4    (float3(scale, scale, scale));
    auto mres       = mul(mtranslate, mscale);
    
    hrMeshInstance(scene, testModel, mres.L(), remapList.data(), remapList.size());
    
    mtranslate = LiteMath::translate4x4(float3(0.0f, 100.0f, 0.0f));
    hrLightInstance(scene, directLight, mtranslate.L());
  }
  hrSceneClose(scene);
  
  
  hrRenderOpen(renderRef, HR_WRITE_DISCARD);
  {
    pugi::xml_node node = hrRenderParamNode(renderRef);
    
    node.append_child(L"width").text()  = 1024;
    node.append_child(L"height").text() = 1024;
    
    node.append_child(L"method_primary").text()   = L"pathtracing";
    node.append_child(L"method_secondary").text() = L"pathtracing";
    node.append_child(L"method_tertiary").text()  = L"pathtracing";
    node.append_child(L"method_caustic").text()   = L"pathtracing";
    node.append_child(L"shadows").text()          = L"1";
    
    node.append_child(L"trace_depth").text()      = 8;
    node.append_child(L"diff_trace_depth").text() = 4;
    node.append_child(L"maxRaysPerPixel").text()  = 256;
  }
  hrRenderClose(renderRef);
  
  ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
  
  hrFlush(scene, render, camRef);
  
  while (true)
  {
    std::this_thread::sleep_for(std::chrono::milliseconds(500));
    
    HRRenderUpdateInfo info = hrRenderHaveUpdate(render);
    
    if (info.haveUpdateFB)
    {
      auto pres = std::cout.precision(2);
      std::cout << "rendering progress = " << info.progress << "% \r";
      std::cout.precision(pres);
    }
    
    if (info.finalUpdate)
      break;
  }
  
  const std::wstring imagePathW = clsp_s2ws(a_imagePath);
  hrRenderSaveFrameBufferLDR(render, imagePathW.c_str());
}
