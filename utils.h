//
// Created by frol on 4/25/19.
//

#ifndef CLSP_UTILS_H
#define CLSP_UTILS_H

#include <vector>

struct SimpleMesh
{
  SimpleMesh()                                  = default;
  SimpleMesh(SimpleMesh&& a_in)                 = default;
  SimpleMesh(const SimpleMesh& a_in)            = default;
  SimpleMesh& operator=(SimpleMesh&& a_in)      = default;
  SimpleMesh& operator=(const SimpleMesh& a_in) = default;
  
  std::vector<float> vPos;
  std::vector<float> vNorm;
  std::vector<float> vTexCoord;
  std::vector<int>   triIndices;
  std::vector<int>   matIndices;
};


SimpleMesh CreatePlane(float a_size = 1.0f);

#include "../HydraAPI/hydra_api/HydraAPI.h"
#include "../HydraAPI/hydra_api/LiteMath.h"


HRMeshRef HRMeshFromSimpleMesh(const wchar_t* a_name, const SimpleMesh& a_mesh, int a_matId);

#endif //CLSP_UTILS_H
