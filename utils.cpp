//
// Created by frol on 4/25/19.
//

#include "utils.h"

SimpleMesh CreatePlane(float a_size)
{
  SimpleMesh plane;
  
  plane.vPos.resize(4 * 4);
  plane.vNorm.resize(4 * 4);
  plane.vTexCoord.resize(4 * 2);
  plane.triIndices.resize(6);
  plane.matIndices.resize(2);
  
  // plane pos, pos is float4
  //
  plane.vPos[0 * 4 + 0] = -a_size;
  plane.vPos[0 * 4 + 1] = 0.0f;
  plane.vPos[0 * 4 + 2] = -a_size;
  plane.vPos[0 * 4 + 3] = 1.0f; // must be 1
  
  plane.vPos[1 * 4 + 0] = a_size;
  plane.vPos[1 * 4 + 1] = 0.0f;
  plane.vPos[1 * 4 + 2] = -a_size;
  plane.vPos[1 * 4 + 3] = 1.0f; // must be 1
  
  plane.vPos[2 * 4 + 0] = a_size;
  plane.vPos[2 * 4 + 1] = 0.0f;
  plane.vPos[2 * 4 + 2] = a_size;
  plane.vPos[2 * 4 + 3] = 1.0f; // must be 1
  
  plane.vPos[3 * 4 + 0] = -a_size;
  plane.vPos[3 * 4 + 1] = 0.0f;
  plane.vPos[3 * 4 + 2] = a_size;
  plane.vPos[3 * 4 + 3] = 1.0f; // must be 1
  
  // plane normals, norm is float4
  //
  plane.vNorm[0 * 4 + 0] = 0.0f;
  plane.vNorm[0 * 4 + 1] = 1.0f;
  plane.vNorm[0 * 4 + 2] = 0.0f;
  plane.vNorm[0 * 4 + 3] = 0.0f; // must be 1
  
  plane.vNorm[1 * 4 + 0] = 0.0f;
  plane.vNorm[1 * 4 + 1] = 1.0f;
  plane.vNorm[1 * 4 + 2] = 0.0f;
  plane.vNorm[1 * 4 + 3] = 0.0f; // must be 1
  
  plane.vNorm[2 * 4 + 0] = 0.0f;
  plane.vNorm[2 * 4 + 1] = 1.0f;
  plane.vNorm[2 * 4 + 2] = 0.0f;
  plane.vNorm[2 * 4 + 3] = 0.0f; // must be 1
  
  plane.vNorm[3 * 4 + 0] = 0.0f;
  plane.vNorm[3 * 4 + 1] = 1.0f;
  plane.vNorm[3 * 4 + 2] = 0.0f;
  plane.vNorm[3 * 4 + 3] = 0.0f; // must be 1
  
  // plane texture coords
  //
  plane.vTexCoord[0 * 2 + 0] = 0.0f;
  plane.vTexCoord[0 * 2 + 1] = 0.0f;
  
  plane.vTexCoord[1 * 2 + 0] = 1.0f;
  plane.vTexCoord[1 * 2 + 1] = 0.0f;
  
  plane.vTexCoord[2 * 2 + 0] = 1.0f;
  plane.vTexCoord[2 * 2 + 1] = 1.0f;
  
  plane.vTexCoord[3 * 2 + 0] = 0.0f;
  plane.vTexCoord[3 * 2 + 1] = 1.0f;
  
  // construct triangles
  //
  plane.triIndices[0] = 0;
  plane.triIndices[1] = 1;
  plane.triIndices[2] = 2;
  
  plane.triIndices[3] = 0;
  plane.triIndices[4] = 2;
  plane.triIndices[5] = 3;
  
  // assign material per triangle
  //
  plane.matIndices[0] = 0;
  plane.matIndices[1] = 0;
  
  return plane;
}


HRMeshRef HRMeshFromSimpleMesh(const wchar_t* a_name, const SimpleMesh& a_mesh, int a_matId)
{
  HRMeshRef meshRef = hrMeshCreate(a_name);
  
  hrMeshOpen(meshRef, HR_TRIANGLE_IND3, HR_WRITE_DISCARD);
  {
    hrMeshVertexAttribPointer4f(meshRef, L"pos", &a_mesh.vPos[0]);
    hrMeshVertexAttribPointer4f(meshRef, L"norm", &a_mesh.vNorm[0]);
    hrMeshVertexAttribPointer2f(meshRef, L"texcoord", &a_mesh.vTexCoord[0]);
    
    hrMeshMaterialId(meshRef, a_matId);
    hrMeshAppendTriangles3(meshRef, int(a_mesh.triIndices.size()), &a_mesh.triIndices[0]);
  }
  hrMeshClose(meshRef);
  
  return meshRef;
}