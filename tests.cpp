#include "clsp_lib_src/clsp.h"
#include "clsp_lib_src/clsp_hydra.h"

#include <iostream>
#include <stdio.h>

void test_insert_materials(CSLP_Context* db)
{
  CSLP_Material              mat;
  std::vector<CSLP_Material> mats;

  mat.dbId   = -1;
  mat.name   = "test1";
  mat.type   = "wood";
  mat.target = "";
  mats.push_back(mat);

  mat.name   = "test2";
  mat.type   = "plastic";
  mats.push_back(mat);

  mat.name   = "test3";
  mat.type   = "plastic";
  mats.push_back(mat);

  mat.name   = "test4";
  mat.type   = "metal";
  mats.push_back(mat);

  clspInsert(db, mats);
}

void test_select_materials(CSLP_Context* db)
{
  auto materials = clspSelectMaterialsWhere(db, "1");
  std::cout << "materials.size() = " << materials.size() << std::endl;
  for(const auto& m : materials)
    std::cout << m.dbId << "\t" << m.name.c_str() << "\t" << m.type.c_str() << std::endl;
}

void test_insert_meshes(CSLP_Context* db)
{
  std::vector<std::string> matNamesList;
  matNamesList.push_back("wood");
  matNamesList.push_back("plastic");
  matNamesList.push_back("glass");

  CSLP_3DModel meshInfo;

  meshInfo.dbId      = -1;
  meshInfo.filepath  = "models/furniture/chairs/chair01.vsgfc";

  meshInfo.boxmin[0] = -1.0f;
  meshInfo.boxmin[1] = -1.0f;
  meshInfo.boxmin[2] = -1.0f;
  meshInfo.boxmax[0] = +1.0f;
  meshInfo.boxmax[1] = +1.0f;
  meshInfo.boxmax[2] = +1.0f;

  meshInfo.category    = "chairs";
  meshInfo.subcategory = "unknown";
  meshInfo.style       = "home";
  meshInfo.matnames    = matNamesList;

  std::vector<CSLP_3DModel> models;

  models.push_back(meshInfo);

  meshInfo.filepath  = "models/furniture/chairs/chair02.vsgfc";
  meshInfo.style     = "office";
  models.push_back(meshInfo);

  meshInfo.filepath  = "models/furniture/chairs/chair03.vsgfc";
  meshInfo.style     = "office";
  models.push_back(meshInfo);

  meshInfo.filepath  = "models/furniture/chairs/chair04.vsgfc";
  meshInfo.style     = "hospital";
  models.push_back(meshInfo);

  meshInfo.filepath  = "models/furniture/tables/table01.vsgfc";
  meshInfo.style     = "home";
  meshInfo.category  = "tables";
  models.push_back(meshInfo);

  clspInsert(db, models);
}

void test_select_models(CSLP_Context* db)
{
  float bSize[3] = {0.075, 0.2, 0.075};
  
  //auto models2 = clspFitInBBox(clspSelect3DModelsWhere(db, "1"), bSize);
  //auto models2 = clspSelect3DModels_Where_Fit_In(db, "category = 'figures'", bSize);
  auto models2 = clspSelect3DModels_Where_Fit_In(db, "1", bSize);
  
  std::cout << "models2.size() = " << models2.size() << std::endl;
  
  for(const auto& o : models2)
    std::cout << o.dbId << "\t" << o.filepath.c_str() << "\t" << o.category.c_str() << "\t" << o.matnames.size() << std::endl;
}

