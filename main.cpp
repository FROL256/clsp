#include "clsp_lib_src/clsp.h"
#include "clsp_lib_src/clsp_hydra.h"
#include "clsp_lib_src/clsp_random.h"
#include "clsp_lib_src/ConsoleColor.h"

#include "utils.h"
#include "rand_model.h"

#include <iostream>
#include <fstream>
#include <sstream>
#include <iomanip>
#include <stdio.h>

#include <chrono>
#include <thread>

#include "../HydraAPI/hydra_api/LiteMath.h"
#include "../HydraAPI/hydra_api/HydraXMLHelpers.h"
#include "../HydraAPI/hydra_api/HydraXMLVerify.h"

void test_insert_materials(CSLP_Context* db);
void test_select_materials(CSLP_Context* db);

void test_insert_meshes(CSLP_Context* db);
void test_select_models(CSLP_Context* db);

void InfoCallBack(const wchar_t* message, const wchar_t* callerPlace, HR_SEVERITY_LEVEL a_level)
{
  if (a_level >= HR_SEVERITY_WARNING)
  {
    #ifdef WIN32
    if (a_level == HR_SEVERITY_WARNING)
      std::wcerr << yellow << L"WARNING: " << callerPlace << L": " << message << white; // << std::endl;
    else
      std::wcerr << red << L"ERROR  : " << callerPlace << L": " << message << white; // << std::endl;
    #else
    if (a_level == HR_SEVERITY_WARNING)
      std::wcerr << L"WARNING: " << callerPlace << L": " << message; // << std::endl;
    else
      std::wcerr << L"ERROR  : " << callerPlace << L": " << message; // << std::endl;
    #endif
  }
}

void clspDebugTestCall(CSLP_Context* a_pDB, const char* a_data);

int main(int argc, char* argv[]) //#NOTE: assume your working directoty is "CLSP/database"
{
  hrInfoCallback(&InfoCallBack);
  //auto testMaterials = clspObtainMaterialsFromHydraSceneLibrary("./statex_00001.xml");
  //std::cout << "testMaterials.size() = " << testMaterials.size() << std::endl;

  /*
  if (argc >= 3)
  {
    const char* model = argv[1];
    const char* image = argv[2];
    test_model(model, image);
    return 0;
  }
  else
  {
    std::cout << "ERROR  : argc = " << argc << std::endl;
    std::cout << "ERROR  : argc must be >= 3 " << argc << std::endl;
    std::cout << "EXAMPLE: 'testmodel.exe model.vsgfc image.png'" << std::endl;
    return 0;
  }
  */

  /////////////////////////////////////////////////////////////////////////////////////////////
  // Add models to database.
  /////////////////////////////////////////////////////////////////////////////////////////////
  

  CSLP_Context* db                   = clspDBCreate("test.db"); 
  auto testMaterials                 = clspObtainMaterialsFromHydraSceneLibrary("./statex_00001.xml");
  clspInsert(db, testMaterials);
  std::cout << "testMaterials.size() = " << testMaterials.size() << std::endl;
  auto testModels                    = clspObtainModelsFromFolder("models");
  clspInsert(db, testModels);
  auto envirs                        = clspListEnvirs("/media/frol/Data_450/envlib");
  clspInsert(db, envirs);
  return 0;

  /////////////////////////////////////////////////////////////////////////////////////////////

  
  /*CSLP_Context* db = clspDBOpen("test.db"); //#NOTE: assume your working directoty is "CLSP/database"
  
  if(db == nullptr)
  {
    std::cout << "[ERROR]: can't open database" << std::endl;
    return 0;
  }
  */
  
  /*
  std::cout << std::endl;
  test_select_materials(db);
  
  std::cout << std::endl;
  std::cout << "cats:" << std::endl;
  auto catlist = clspList3DModelsCategories(db);
  for(const auto& cat : catlist)
    std::cout << cat.c_str() << std::endl;
  
  std::cout << std::endl;
  test_select_models(db);
  */
  
  //clspDBClose(db); db = nullptr;
 

  //test_model_m("models/cg_common/dragon.vsgfc", "outimages/test.png");
  //test_model("models/figures/torus.vsgfc", "outimages/test.png");
  //test_model("models/ComputerCasePC/ComputerCasePC_00001.vsgfc", "outimages/test.png"); 
  
  //////////////////////////////////////////////////////
  //Preview models in database
  //

  //
    //ModelGenSettings settings;
  
    //settings.imagePath     = "outimages/test";
    //settings.sqlSuffix     = "category != 'human' and category != 'cg_common' and category != 'WhiteBoard' \ "
    //                         "and category != 'TvSet' and category != 'MonitorsPC' and category != 'figures' and \
    //                         id != 54 and id != 55 and id != 220 and id != 221 and id != 83 and id != 21";
    ////settings.sqlSuffix     = "category == 'Table' or category == 'ChairHome' or category == 'ChairOffice' \ "
    ////                         "or category == 'BagsCasesBackpacks' or category == 'TableOffice' or \ "
    ////                         "category == 'Wardrobe' or category == 'Pillow' or category == 'StorageBox' ";
    //settings.floorType         = FLOOR_GREY;
    //settings.lightingType      = LIGHT_DEFAULT;
    //settings.numOfExamples     = 5;
    //settings.floorSizeInMeters = 20.0f;
    //settings.spp               = 128;
  
    //test_model_m2(settings);

  
  
  //////////////////////////////////////////////////////

  //#ifdef WIN32
  //  int x = 0;
  //  std::cin >> x;
  //#endif
  return 0;
}

