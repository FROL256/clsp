#include "clsp_lib_src/clsp.h"
#include "clsp_lib_src/clsp_hydra.h"
#include "clsp_lib_src/clsp_random.h"

#include "utils.h"
#include "rand_model.h"

#include <iostream>
//#include <fstream>
//#include <sstream>
#include <iomanip>
//#include <stdio.h>

#include <chrono>
#include <thread>

#include "../HydraAPI/hydra_api/LiteMath.h"
#include "../HydraAPI/hydra_api/HydraXMLHelpers.h"
#include "../HydraAPI/hydra_api/HydraXMLVerify.h"

static const float DEG_TO_RAD = 0.01745329251f; // float(3.14159265358979323846f) / 180.0f;

using HydraLiteMath::float3;
using HydraLiteMath::float4;
using HydraLiteMath::float4x4;

unsigned int g_table[hr_qmc::QRNG_DIMENSIONS][hr_qmc::QRNG_RESOLUTION];

void test_model_gen(ModelGenSettings a_settings)
{
  hr_qmc::init(g_table);
  
  hrSceneLibraryOpen(L"temp", HR_WRITE_DISCARD);
  
  HRRenderRef    render; render.id = 0;
  
  HRMaterialRef matOfPlane     = hrMaterialCreate(L"PlaneMat");
  HRMaterialRef matOfTestModel = hrMaterialCreate(L"RedMaterial");
  
  HRTextureNodeRef texFloor    = hrTexture2DCreateFromFile(L"Textures_lib/10cm.jpg");
  
  HRTextureNodeRef texEnv, texBack;
  
  if(a_settings.lightingType == LIGHT_DEFAULT)
  {
    texEnv  = hrTexture2DCreateFromFile(L"Textures_lib/Envir/EnvMatEdit.hdr");
    texBack = hrTexture2DCreateFromFile(L"Textures_lib/Envir/Back.jpg");
  }
  else
  {
    texBack = hrTexture2DCreateFromFile(L"Textures_lib/Envir/Brooklyn_Bridge_Planks_2k.hdr");
    texEnv  = hrTexture2DCreateFromFile(L"Textures_lib/Envir/20_Subway_Lights_3k.hdr");
  }
  
  hrMaterialOpen(matOfPlane, HR_WRITE_DISCARD);
  {
    auto matNode = hrMaterialParamNode(matOfPlane);
    
    if(a_settings.floorType == FLOOR_GREY)
    {
      auto diff    = matNode.append_child(L"diffuse");
      diff.append_attribute(L"brdf_type").set_value(L"lambert");
      diff.append_child(L"color").append_attribute(L"val") = L"1.0 1.0 1.0";
      diff.append_attribute(L"tex_apply_mode").set_value(L"replace");
      
      auto texNode = hrTextureBind(texFloor, diff);
      
      texNode.append_attribute(L"matrix");
      
      const float s = 5.0f*a_settings.floorSizeInMeters;
      
      float samplerMatrix[16] = {s, 0, 0, 0,
                                 0, s, 0, 0,
                                 0, 0, 1, 0,
                                 0, 0, 0, 1};
      
      texNode.append_attribute(L"addressing_mode_u").set_value(L"wrap");
      texNode.append_attribute(L"addressing_mode_v").set_value(L"wrap");
      texNode.append_attribute(L"input_gamma").set_value(2.2f);
      texNode.append_attribute(L"input_alpha").set_value(L"rgb");
      
      HydraXMLHelpers::WriteMatrix4x4(texNode, L"matrix", samplerMatrix);
    }
    else if(a_settings.floorType == FLOOR_SHADOW_MATTE)
    {
      matNode.attribute(L"type") = L"shadow_catcher";
      
      auto opacity = matNode.append_child(L"opacity");
      opacity.append_child(L"skip_shadow").append_attribute(L"val").set_value(1);
      
      VERIFY_XML(matNode);
    }
  }
  hrMaterialClose(matOfPlane);
  
  hrMaterialOpen(matOfTestModel, HR_WRITE_DISCARD);
  {
    auto matNode = hrMaterialParamNode(matOfTestModel);
    auto diff    = matNode.append_child(L"diffuse");
    
    diff.append_attribute(L"brdf_type").set_value(L"lambert");
    diff.append_child(L"color").append_attribute(L"val") = L"0.5 0.05 0.05";
  }
  hrMaterialClose(matOfTestModel);
  
  
  /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
  // Meshes
  /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
  
  HRMeshRef planeRef = HRMeshFromSimpleMesh(L"my_plane", CreatePlane(a_settings.floorSizeInMeters), matOfPlane.id);
  
  /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
  // Light
  /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
  
  HRLightRef sky = hrLightCreate(L"sky");
  
  
  hrLightOpen(sky, HR_WRITE_DISCARD);
  {
    auto lightNode = hrLightParamNode(sky);
    
    lightNode.attribute(L"type").set_value(L"sky");
    lightNode.attribute(L"distribution").set_value(L"map");
    
    auto intensityNode = lightNode.append_child(L"intensity");
    
    intensityNode.append_child(L"color").append_attribute(L"val").set_value(L"1 1 1");
    intensityNode.append_child(L"multiplier").append_attribute(L"val").set_value(L"1.0");
    
    auto texNode = hrTextureBind(texEnv, intensityNode.child(L"color"));
    
    if(a_settings.lightingType != LIGHT_RAND_ENV)
    {
      auto back = lightNode.append_child(L"back");
      auto tex  = hrTextureBind(texBack, back);
      
      int w,h,bpp;
      hrTexture2DGetSize(texBack, &w, &h, &bpp);
      
      if(a_settings.lightingType == LIGHT_RAND_ENV_AND_BACK)
      {
        back.append_attribute(L"mode")       = L"spherical";
        tex.append_attribute(L"input_gamma") = (bpp == 4) ? 2.2f : 1.0f;
        //back.append_attribute(L"multcolor")  = L"1 0 0";
      }
      else
      {
        back.append_attribute(L"mode")       = L"camera_mapped"; // or spherical
        tex.append_attribute(L"input_gamma") = (bpp == 4) ? 2.2f : 1.0f;
      }
      
      //// well, due to enviromnet texture coordinates are related to polar coordinates,
      //// environment rotate can be implemented via shift matrix. So, this is not classic rotation matrix.
      ////
      texNode.append_attribute(L"matrix");
      float samplerMatrix[16] = {1, 0, 0, 0.5, // { 1, 0, 0, offset,
                                 0, 1, 0, 0,
                                 0, 0, 1, 0,
                                 0, 0, 0, 1};
      
      texNode.append_attribute(L"addressing_mode_u").set_value(L"wrap");
      texNode.append_attribute(L"addressing_mode_v").set_value(L"wrap");
      
      HydraXMLHelpers::WriteMatrix4x4(texNode, L"matrix", samplerMatrix);
    }
    VERIFY_XML(lightNode);
  }
  hrLightClose(sky);
  
  ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
  ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
  
  // set camera
  //
  HRCameraRef camRef    = hrCameraCreate(L"mycamera");
  HRSceneInstRef scene  = hrSceneCreate(L"myscene");
  HRRenderRef renderRef = hrRenderCreate(L"HydraModern"); // opengl1
  
  hrRenderEnableDevice(renderRef, 0, true);
  
  ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
  ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
  
  ICLSP_Randomizer* pRandomizer = CreateCLSPRandomizer(".");
  
  float bboxSize[3] = {3,3,3};
  
  // Select models by some condition from database
  std::vector<CSLP_3DModel> models = pRandomizer->SelectModels_WhereFitIn(a_settings.sqlSuffix, bboxSize);
  auto gen = hr_prng::RandomGenInit(0); // or Sobol
  
  //// out train and check dataset
  //{
  //  std::ofstream foutTrain("models_train.txt");
  //  std::ofstream foutTest ("models_test.txt");
  //
  //  for(size_t i=0;i<models.size();i++)
  //  {
  //    if(i%5 == 0)
  //      foutTest << models[i].dbId << std::endl;
  //    else
  //      foutTrain << models[i].dbId << std::endl;
  //  }
  //
  //}
  
  
  std::cout << "models num = " << models.size() << std::endl;
  
  ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
  ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
  
  for (int modelId = 0; modelId < models.size(); modelId++)
  {
    std::wstring pathW  = clsp_s2ws(models[modelId].filepath);
    HRMeshRef testModel = hrMeshCreateFromFileDL(pathW.c_str());
    std::cout << "Model path: " << models[modelId].filepath.c_str() << std::endl;
  
    std::vector<float> rndLight(RND_DIM_TOTAL_SIZE);
    std::vector<float> rndArray(ICLSP_Randomizer::numRndDigit);
    
    for(int sampleNum=0;sampleNum<a_settings.numOfExamples;sampleNum++)
    {
      for (int i = 0; i < rndArray.size(); i++)
        rndArray[i] = hr_prng::rndFloat(&gen);
  
      for (int i = 0; i < rndLight.size(); i++)
        rndLight[i] = hr_prng::rndFloat(&gen); //hr_qmc::rndFloat(sampleNum, i, &g_table[0][0]);
      
      const auto remapList = pRandomizer->SampleRemapList(models[modelId], testModel, rndArray);
  
      /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
      // Camera
      /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
  
      const float3 bBoxMax = {models[modelId].boxmax[0], models[modelId].boxmax[1], models[modelId].boxmax[2]};
      const float3 bBoxMin = {models[modelId].boxmin[0], models[modelId].boxmin[1], models[modelId].boxmin[2]};
      const float sizeBbox = HydraLiteMath::length(bBoxMax - bBoxMin);
 
      float3 camPos(0, sizeBbox * 1.0f, sizeBbox * 1.4f);
      float3 camTar(0, 0.5f*models[modelId].boxmax[1], 0);
      
      {
        float4x4 mRotY = HydraLiteMath::rotate_Y_4x4(rndLight[RND_DIM_CAMERA_ROT] * 180.0f * DEG_TO_RAD);
        float4x4 mRotZ = HydraLiteMath::rotate_Z_4x4((rndLight[RND_DIM_CAMERA_ANG] *100.0f - 45.0f) * DEG_TO_RAD);
      
        camPos = mul(mul(mRotZ, mRotY), camPos);
        camTar = mul(mul(mRotZ, mRotY), camTar);
      }
      
      std::wstring cameraPosStr = std::to_wstring(camPos.x) + L" " + std::to_wstring(camPos.y) + L" " + std::to_wstring(camPos.z);
      std::wstring targetPosStr = std::to_wstring(camTar.x) + L" " + std::to_wstring(camTar.y) + L" " + std::to_wstring(camTar.z);
  
      hrCameraOpen(camRef, HR_WRITE_DISCARD);
      {
        auto camNode = hrCameraParamNode(camRef);
    
        camNode.append_child(L"fov").text().set(L"45");
        camNode.append_child(L"nearClipPlane").text().set(L"0.01");
        camNode.append_child(L"farClipPlane").text().set(L"100.0");
    
        camNode.append_child(L"up").text()       = L"0 1 0";
        camNode.append_child(L"position").text() = cameraPosStr.c_str();
        camNode.append_child(L"look_at").text()  = targetPosStr.c_str();
      }
      hrCameraClose(camRef);
  
      //////////////////////////////////////////////////////////////////////////////////////////////////////////////////
      
      float3 envColor(rndLight[RND_DIM_ENV_COLOR_R], rndLight[RND_DIM_ENV_COLOR_G],rndLight[RND_DIM_ENV_COLOR_B]);
      float  envMult = 1.0f;
      {
        if(rndLight[RND_DIM_ENV_COLOR] > 0.5f)
        {
          envColor = float3(1, 1, 1);
          envMult  = 1.0f;
        }
        else
        {
          envColor.x = fmax(0.25f, envColor.x);
          envColor.y = fmax(0.25f, envColor.y);
          envColor.z = fmax(0.25f, envColor.z);
          envMult    = fmax(rndLight[RND_DIM_ENV_COLOR], 0.001f)*2.0f;
        }
      }
      
      float3 envColor2 = envColor*envMult;
  
      float3 bakColor(rndLight[RND_DIM_BAK_COLOR_R], rndLight[RND_DIM_BAK_COLOR_G],rndLight[RND_DIM_BAK_COLOR_B]);
      float  bakMult = 1.0f;
      {
        if(rndLight[RND_DIM_BAK_MULT] > 0.5f)
        {
          bakColor = float3(1, 1, 1);
          bakMult  = 1.0f;
        }
        else
        {
          bakColor.x = fmax(0.25f, bakColor.x);
          bakColor.y = fmax(0.25f, bakColor.y);
          bakColor.z = fmax(0.25f, bakColor.z);
          bakMult    = fmax(rndLight[RND_DIM_BAK_MULT], 0.001f)*2.0f;
        }
      }
      bakColor = bakColor*bakMult;
  
      if(rndLight[RND_DIM_SELECT_WHITE] < 0.5f)
      {
        envColor  = float3(1,1,1);
        bakColor  = float3(1,1,1);
        envColor2 = envColor;
        bakMult = 1.0f;
        envMult = 1.0f;
      }
      //////////////////////////////////////////////////////////////////////////////////////////////////////////////////
      
      auto envColorStr  = std::to_wstring(envColor.x) + L" " + std::to_wstring(envColor.y) + L" " + std::to_wstring(envColor.z);
      auto bakColorStr  = std::to_wstring(bakColor.x) + L" " + std::to_wstring(bakColor.y) + L" " + std::to_wstring(bakColor.z);
      auto envColorStr2 = std::to_wstring(envColor2.x) + L" " + std::to_wstring(envColor2.y) + L" " + std::to_wstring(envColor2.z);
      
      hrLightOpen(sky, HR_WRITE_DISCARD);
      {
        auto lightNode     = hrLightParamNode(sky);
  
        lightNode.attribute(L"type").set_value(L"sky");
        lightNode.attribute(L"distribution").set_value(L"map");
        
        auto intensityNode = lightNode.append_child(L"intensity");
  
        intensityNode.append_child(L"color").append_attribute(L"val")      = envColorStr.c_str();
        intensityNode.append_child(L"multiplier").append_attribute(L"val") = envMult;
        
        auto texNodeEnv = hrTextureBind(texEnv, intensityNode.child(L"color"));
        
        VERIFY_XML(lightNode);
      }
      hrLightClose(sky);
      
      /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
      // Create scene
      /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
  
      hrSceneOpen(scene, HR_WRITE_DISCARD);
      {
        const float scale = 1.0f;
        auto mtranslate = HydraLiteMath::translate4x4(float3(0, -scale * models[modelId].boxmin[1], 0));
        auto mscale     = HydraLiteMath::scale4x4(float3(scale, scale, scale));
        auto mRot       = HydraLiteMath::rotate_Y_4x4(rndLight[RND_DIM_MODEL_ROT]*180.0f*DEG_TO_RAD);
        auto mres       = HydraLiteMath::mul(mtranslate, mRot);
        hrMeshInstance(scene, testModel, mres.L(), remapList.data(), remapList.size());
    
        mtranslate.identity();
        mRot.identity();
        mscale = HydraLiteMath::scale4x4(float3(scale, scale, -scale));
        mres   = mul(mtranslate, mscale);
        hrMeshInstance(scene, planeRef, mres.L());
    
        mtranslate.identity();
        hrLightInstance(scene, sky, mtranslate.L());
      }
      hrSceneClose(scene);
  
      /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
      // Render settings
      /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
  
      hrRenderOpen(renderRef, HR_WRITE_DISCARD);
      {
        pugi::xml_node node = hrRenderParamNode(renderRef);
    
        node.append_child(L"width").text() = 512;
        node.append_child(L"height").text() = 512;
    
        node.append_child(L"method_primary").text()   = L"pathtracing";
        node.append_child(L"method_secondary").text() = L"pathtracing";
        node.append_child(L"method_tertiary").text()  = L"pathtracing";
        node.append_child(L"method_caustic").text()   = L"none";
    
        //node.append_child(L"qmc_variant").text()      = 7;
        node.append_child(L"offline_pt").text() = 1;
    
        node.append_child(L"trace_depth").text()      = 6;
        node.append_child(L"diff_trace_depth").text() = 3;
        node.append_child(L"maxRaysPerPixel").text()  = a_settings.spp;
        node.append_child(L"evalgbuffer").text()      = 0;
      }
      hrRenderClose(renderRef);
  
      ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
  
      hrFlush(scene, renderRef, camRef);
  
      while (true)
      {
        std::this_thread::sleep_for(std::chrono::milliseconds(500));
    
        HRRenderUpdateInfo info = hrRenderHaveUpdate(render);
    
        if (info.haveUpdateFB)
        {
          auto pres = std::cout.precision(2);
          std::cout << "rendering progress = " << info.progress << "% \r";
          std::cout.precision(pres);
        }
    
        if (info.finalUpdate)
          break;
      }
  
      {
        std::wstringstream namestream;
        namestream << std::fixed << a_settings.imagePath << std::setfill(L"0"[0]) << std::setw(3)
                   << models[modelId].dbId
                   << "_" << sampleNum << L"_AA.png";
        const std::wstring imagePathW = namestream.str();
    
        hrRenderSaveFrameBufferLDR(render, imagePathW.c_str());
      }
  
      ///////////////////////////////////////////////////////////////////////////////////////////////////// AB
      
      hrLightOpen(sky, HR_WRITE_DISCARD);
      {
        auto lightNode = hrLightParamNode(sky);
    
        lightNode.attribute(L"type").set_value(L"sky");
        lightNode.attribute(L"distribution").set_value(L"map");
    
        auto intensityNode = lightNode.append_child(L"intensity");
    
        intensityNode.append_child(L"color").append_attribute(L"val")      = envColorStr.c_str();
        intensityNode.append_child(L"multiplier").append_attribute(L"val") = envMult;
  
        auto texNodeEnv = hrTextureBind(texEnv, intensityNode.child(L"color"));
        auto nodeBack   = lightNode.append_child(L"back"); // .attribute(L"multcolor")
  
        nodeBack.append_attribute(L"mode")      = L"spherical";
        nodeBack.append_attribute(L"multcolor") = bakColorStr.c_str();
        
        int w,h,bpp;
        hrTexture2DGetSize(texBack, &w, &h, &bpp);
        
        auto texNodeBack = hrTextureBind(texBack, nodeBack);
        texNodeBack.append_attribute(L"input_gamma") = (bpp == 4) ? 2.2f : 1.0f;
        
        VERIFY_XML(lightNode);
      }
      hrLightClose(sky);
      
      hrFlush(scene, renderRef, camRef);
  
      while (true)
      {
        std::this_thread::sleep_for(std::chrono::milliseconds(500));
    
        HRRenderUpdateInfo info = hrRenderHaveUpdate(render);
    
        if (info.haveUpdateFB)
        {
          auto pres = std::cout.precision(2);
          std::cout << "rendering progress = " << info.progress << "% \r";
          std::cout.precision(pres);
        }
    
        if (info.finalUpdate)
          break;
      }
  
      {
        std::wstringstream namestream;
        namestream << std::fixed << a_settings.imagePath << std::setfill(L"0"[0]) << std::setw(3)
                   << models[modelId].dbId
                   << "_" << sampleNum << L"_AB.png";
        const std::wstring imagePathW = namestream.str();
    
        hrRenderSaveFrameBufferLDR(render, imagePathW.c_str());
      }
      
      ///////////////////////////////////////////////////////////////////////////////////////////////////// BA
      
      hrLightOpen(sky, HR_WRITE_DISCARD);
      {
        auto lightNode = hrLightParamNode(sky);
    
        lightNode.attribute(L"type").set_value(L"sky");
        lightNode.attribute(L"distribution").set_value(L"map");
    
        auto intensityNode = lightNode.append_child(L"intensity");
    
        intensityNode.append_child(L"color").append_attribute(L"val")      = bakColorStr.c_str();
        intensityNode.append_child(L"multiplier").append_attribute(L"val") = 1.0f;
    
        auto texNodeEnv = hrTextureBind(texBack, intensityNode.child(L"color"));
        auto nodeBack   = lightNode.append_child(L"back"); // .attribute(L"multcolor")
    
        nodeBack.append_attribute(L"mode")      = L"spherical";
        nodeBack.append_attribute(L"multcolor") = envColorStr2.c_str();
    
        int w,h,bpp;
        hrTexture2DGetSize(texEnv, &w, &h, &bpp);
    
        auto texNodeBack = hrTextureBind(texEnv, nodeBack);
        texNodeBack.append_attribute(L"input_gamma") = (bpp == 4) ? 2.2f : 1.0f;
    
        VERIFY_XML(lightNode);
      }
      hrLightClose(sky);
  
      hrFlush(scene, renderRef, camRef);
  
      while (true)
      {
        std::this_thread::sleep_for(std::chrono::milliseconds(500));
    
        HRRenderUpdateInfo info = hrRenderHaveUpdate(render);
    
        if (info.haveUpdateFB)
        {
          auto pres = std::cout.precision(2);
          std::cout << "rendering progress = " << info.progress << "% \r";
          std::cout.precision(pres);
        }
    
        if (info.finalUpdate)
          break;
      }
  
      {
        std::wstringstream namestream;
        namestream << std::fixed << a_settings.imagePath << std::setfill(L"0"[0]) << std::setw(3)
                   << models[modelId].dbId
                   << "_" << sampleNum << L"_BA.png";
        const std::wstring imagePathW = namestream.str();
    
        hrRenderSaveFrameBufferLDR(render, imagePathW.c_str());
      }
  
      ///////////////////////////////////////////////////////////////////////////////////////////////////// BB
  
      hrLightOpen(sky, HR_WRITE_DISCARD);
      {
        auto lightNode = hrLightParamNode(sky);
    
        lightNode.attribute(L"type").set_value(L"sky");
        lightNode.attribute(L"distribution").set_value(L"map");
    
        auto intensityNode = lightNode.append_child(L"intensity");
    
        intensityNode.append_child(L"color").append_attribute(L"val")      = bakColorStr.c_str();
        intensityNode.append_child(L"multiplier").append_attribute(L"val") = 1.0f;
    
        auto texNodeEnv = hrTextureBind(texBack, intensityNode.child(L"color"));
       
        VERIFY_XML(lightNode);
      }
      hrLightClose(sky);
  
      hrRenderOpen(renderRef, HR_OPEN_EXISTING);
      {
        auto node = hrRenderParamNode(renderRef);
        node.child(L"evalgbuffer").text() = 1;
      }
      hrRenderClose(renderRef);
      
      hrFlush(scene, renderRef, camRef);
  
      while (true)
      {
        std::this_thread::sleep_for(std::chrono::milliseconds(500));
    
        HRRenderUpdateInfo info = hrRenderHaveUpdate(render);
    
        if (info.haveUpdateFB)
        {
          auto pres = std::cout.precision(2);
          std::cout << "rendering progress = " << info.progress << "% \r";
          std::cout.precision(pres);
        }
    
        if (info.finalUpdate)
          break;
      }
  
      {
        std::wstringstream namestream;
        namestream << std::fixed << a_settings.imagePath << std::setfill(L"0"[0]) << std::setw(3)
                   << models[modelId].dbId
                   << "_" << sampleNum;
        
        const std::wstring imagePathW   = namestream.str() + L"_BB.png";
        const std::wstring tcolorPathW  = namestream.str() + L"_XX.png";
        const std::wstring alphaPathW   = namestream.str() + L"_YY.png";
        const std::wstring pencilPathW  = namestream.str() + L"_ZZ.png";
        
        hrRenderSaveFrameBufferLDR(renderRef,  imagePathW.c_str());
        hrRenderSaveGBufferLayerLDR(renderRef, tcolorPathW.c_str(), L"diffcolor");
        hrRenderSaveGBufferLayerLDR(renderRef, alphaPathW.c_str(), L"alpha");
        //hrRenderSaveGBufferLayerLDR(renderRef, pencilPathW.c_str(), L"coverage");

      }
      
      
  
    }
    
    
    
    
  }
}