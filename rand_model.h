//
// Created by frol on 10/4/19.
//

#ifndef CLSP_RAND_MODEL_H
#define CLSP_RAND_MODEL_H

enum FLOOR_TYPE{FLOOR_GREY    = 0, FLOOR_SHADOW_MATTE = 1};
enum LIGHT_TYPE{LIGHT_DEFAULT = 0, LIGHT_RAND_ENV = 1, LIGHT_RAND_ENV_AND_BACK = 2};

struct ModelGenSettings
{
  const char* imagePath;
  const char* sqlSuffix;
  
  FLOOR_TYPE  floorType;
  LIGHT_TYPE  lightingType;
  int         numOfExamples;
  float       floorSizeInMeters;
  int         spp;
};


void test_model_m2(ModelGenSettings a_settings);
void test_model_gen(ModelGenSettings a_settings);

enum RND_LIGHTING_PARAMS{
  RND_DIM_MODEL_ROT    = 0,
  RND_DIM_CAMERA_ROT   = 1,
  RND_DIM_CAMERA_ANG   = 2,
  RND_DIM_ENV_COLOR    = 3,
  RND_DIM_ENV_COLOR_R  = 4,
  RND_DIM_ENV_COLOR_G  = 5,
  RND_DIM_ENV_COLOR_B  = 6,
  RND_DIM_BAK_COLOR_R  = 7,
  RND_DIM_BAK_COLOR_G  = 8,
  RND_DIM_BAK_COLOR_B  = 9,
  RND_DIM_BAK_MULT     = 10,
  RND_DIM_SELECT_WHITE = 11,
  RND_DIM_TOTAL_SIZE   = 12,
};


#endif //CLSP_RAND_MODEL_H
